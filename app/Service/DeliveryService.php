<?php

namespace App\Service;

use App\Models\Delivery;
use App\Models\Inventory;
use App\Models\Location;
use App\Models\Medicine;
use App\Models\PatientType;
use App\Models\Prescription;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class DeliveryService
{
    public function getDeliveryData(Location $location)
    {
        $deliveries = Delivery::where('location_id', $location?->id)->get();
        return ['deliveries' => $deliveries, 'locationName' => $location?->name];
    }

    public function getCreateFormData()
    {

        $medicines = Medicine::select('id', 'name')->orderby('name')->get();

        return ['medicines' => $medicines];
    }

    public function createPrescription($prescriptionData, User $user)
    {
        $prescriptionContent = array_map(function ($medicine) {
            return [
                'medicine_id' => $medicine['medicine_id'],
                'smallest_unit_count' => $medicine['amountInBoxes'] * Medicine::find($medicine['medicine_id'])->amount_of_units_in_one_box,
            ];
        }, $prescriptionData['prescriptionContent']);

        DB::transaction(function () use ($prescriptionData, $user, $prescriptionContent) {
            $prescription = Prescription::create([
                'id_on_paper' => $prescriptionData['id_on_paper'],
                'patient_id' => $prescriptionData['patient_id'],
                'patient_type_id' => $prescriptionData['patient_type'],
                'prescriber_id' => $user->id,
                'location_id' => $user->location_id,
                'note' => $prescriptionData['note'],
            ]);

            $prescription->contents()->createMany($prescriptionContent);

            foreach ($prescriptionContent as $row) {
                $inventoryRow = Inventory::where('medicine_id', $row['medicine_id'])->where('location_id', $user->location_id)->get()->first();
                $inventoryRow->smallest_unit_count -= $row['smallest_unit_count'];
                $inventoryRow->save();
            }
        });
    }
}
