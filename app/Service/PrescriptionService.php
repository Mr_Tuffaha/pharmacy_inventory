<?php

namespace App\Service;

use App\Models\Inventory;
use App\Models\Location;
use App\Models\Medicine;
use App\Models\PatientType;
use App\Models\Prescription;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class PrescriptionService
{
    public function getPrescriptionsList(Location $location)
    {
        $locationName = $location->name;
        $prescriptions = $location->id ? Prescription::where('location_id', $location->id)->get() : [];
        return ['prescriptions' => $prescriptions, 'locationName' => $locationName];
    }

    public function getCreateFormData(Location $location)
    {
        $patientTypes = PatientType::all();

        $invetoryMedicines = Inventory::select('medicine_id', 'smallest_unit_count')->where('location_id', $location->id)->orderby('medicine_id')->get()->filter(function ($medicine) {
            return $medicine->amount_in_boxes > 0;
        });

        $availableKeys = $invetoryMedicines->pluck('medicine_id');

        $availableMedicines = $invetoryMedicines->map(function ($inventoryItem) {
            return [
                'medicine_id' => $inventoryItem->medicine->id,
                'medicine_name' => $inventoryItem->medicine->name,
                'medicine_amount_in_boxes' => $inventoryItem->amount_in_boxes,
            ];
        });


        $availableMedicinesWithKeys = array_combine($availableKeys->toArray(), $availableMedicines->toArray());

        return ['invetoryMedicines' => $availableMedicinesWithKeys, 'patientTypes' => $patientTypes];
    }

    public function createPrescription($prescriptionData, User $user)
    {
        $prescriptionContent = array_map(function ($medicine) {
            return [
                'medicine_id' => $medicine['medicine_id'],
                'smallest_unit_count' => $medicine['amountInBoxes'] * Medicine::find($medicine['medicine_id'])->amount_of_units_in_one_box,
            ];
        }, $prescriptionData['prescriptionContent']);

        DB::transaction(function () use ($prescriptionData, $user, $prescriptionContent) {
            $prescription = Prescription::create([
                'id_on_paper' => $prescriptionData['id_on_paper'],
                'patient_id' => $prescriptionData['patient_id'],
                'patient_type_id' => $prescriptionData['patient_type'],
                'prescriber_id' => $user->id,
                'location_id' => $user->location_id,
                'note' => $prescriptionData['note'],
            ]);

            $prescription->contents()->createMany($prescriptionContent);

            foreach ($prescriptionContent as $row) {
                $inventoryRow = Inventory::where('medicine_id', $row['medicine_id'])->where('location_id', $user->location_id)->get()->first();
                $inventoryRow->smallest_unit_count -= $row['smallest_unit_count'];
                $inventoryRow->save();
            }
        });
    }
}
