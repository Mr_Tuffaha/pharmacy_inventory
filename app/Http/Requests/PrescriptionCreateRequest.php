<?php

namespace App\Http\Requests;

use App\Rules\PrescriptionContentRule;
use Illuminate\Foundation\Http\FormRequest;

class PrescriptionCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_on_paper' => 'required',
            'patient_id' => 'required',
            'patient_type' => 'required|exists:App\Models\PatientType,id',
            'note',
            'prescriptionContent' => ['required', 'array', new PrescriptionContentRule],
        ];
    }
}
