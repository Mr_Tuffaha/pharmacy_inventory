<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\PrescriptionContentRequest;
use App\Models\Prescription;

class PrescriptionContentController extends Controller
{
    //
    public function index(PrescriptionContentRequest $request)
    {
        $prescriptionContent = Prescription::find($request->prescription_id)->contents;
        return response()->json($prescriptionContent);
    }
}
