<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\DeliveryContentRequest;
use App\Models\Delivery;


class DeliveryContentController extends Controller
{
    //
    public function index(DeliveryContentRequest $request)
    {
        $deliveryContent = Delivery::find($request->delivery_id)->content;
        return response()->json($deliveryContent);
    }
}
