<?php

namespace App\Http\Controllers;

use App\Models\Delivery;
use App\Service\DeliveryService;
use Illuminate\Http\Request;

class DeliveryController extends Controller
{
    //

    protected $deliveryService;

    public function __construct(DeliveryService $deliveryService)
    {
        $this->deliveryService = $deliveryService;
    }

    public function index(Request $request)
    {
        $data = $this->deliveryService->getDeliveryData($request->user()->location);
        return view('sb-admin.deliveries.index', $data);
    }

    public function getCreateForm(Request $request)
    {
        $data = $this->deliveryService->getCreateFormData();
        return view('sb-admin.deliveries.create', $data);
    }
}
