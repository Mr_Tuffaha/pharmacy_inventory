<?php

namespace App\Http\Controllers;

use App\Models\Inventory;
use Illuminate\Http\Request;

class InventoryController extends Controller
{
    //

    public function index(Request $request)
    {
        $locationId = $request->user()->location?->id;
        $locationName = $request->user()->location?->name;
        $inventory = $locationId ? Inventory::where('location_id', $locationId)->get() : [];
        return view('sb-admin.inventory', ['inventory' => $inventory, 'locationName' => $locationName]);
    }
}
