<?php

namespace App\Http\Controllers;

use App\Http\Requests\PrescriptionCreateRequest;
use App\Service\PrescriptionService;
use Illuminate\Http\Request;

class PrescriptionController extends Controller
{
    protected $prescriptionService;

    public function __construct(PrescriptionService $prescriptionService)
    {
        $this->prescriptionService = $prescriptionService;
    }
    //
    public function index(Request $request)
    {
        $data = $this->prescriptionService->getPrescriptionsList($request->user()->location);
        return view('sb-admin.prescriptions.index', $data);
    }

    public function getCreateForm(Request $request)
    {
        $data = $this->prescriptionService->getCreateFormData($request->user()->location);
        return view('sb-admin.prescriptions.create', $data);
    }

    public function create(PrescriptionCreateRequest $request)
    {
        $prescriptionData = $request->only([
            'id_on_paper',
            'patient_id',
            'patient_type',
            'note',
            'prescriptionContent'
        ]);

        $this->prescriptionService->createPrescription($prescriptionData, $request->user());

        return redirect(route('prescriptions.index'));
    }
}
