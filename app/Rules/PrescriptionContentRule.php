<?php

namespace App\Rules;

use App\Models\Inventory;
use App\Models\Medicine;
use Illuminate\Contracts\Validation\Rule;

class PrescriptionContentRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $locationId = auth()->user()->location_id;
        foreach ($value as $medicine) {
            if (!Medicine::find($medicine['medicine_id'])) return false;
            $availableAmount = Inventory::where('medicine_id', $medicine['medicine_id'])
                ->where('location_id', $locationId)->get()->first()?->amount_in_boxes;
            if (!$availableAmount) return false;
            if ($medicine['amountInBoxes'] <= 0 || $medicine['amountInBoxes'] > $availableAmount) return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The validation error message.';
    }
}
