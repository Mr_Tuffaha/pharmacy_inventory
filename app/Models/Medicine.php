<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Medicine extends Model
{
    use HasFactory;

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    protected $appends = [
        'amount_of_units_in_one_box',
    ];

    protected $with = ['type'];

    public function type()
    {
        return $this->belongsTo(MedicineType::class, 'medicine_type_id');
    }

    public function getAmountOfUnitsInOneBoxAttribute()
    {
        return $this->sachets_in_box * $this->units_in_sachet;
    }

    public function hasMoreThanOneUnitInOneBox()
    {
        return $this->sachets_in_box * $this->units_in_sachet > 1 ? true : false;
    }
}
