<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Prescription extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_on_paper',
        'patient_id',
        'patient_type_id',
        'location_id',
        'prescriber_id',
        'note',
    ];

    protected $with = [
        'patientType',
        'prescriber'
    ];

    public function patientType()
    {
        return $this->belongsTo(PatientType::class, 'patient_type_id');
    }

    public function contents()
    {
        return $this->hasMany(PrescriptionContent::class);
    }

    public function prescriber()
    {
        return $this->belongsTo(User::class, 'prescriber_id');
    }
}
