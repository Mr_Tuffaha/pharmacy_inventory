<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    use HasFactory;

    protected $with = [
        'medicine'
    ];

    protected $appends = [
        'amount_in_boxes',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function location()
    {
        return $this->belongsTo(Location::class, 'location_id');
    }


    public function medicine()
    {
        return $this->belongsTo(Medicine::class, 'medicine_id');
    }

    public function getAmountInBoxesAttribute()
    {
        $unitsInOneBox = $this->medicine?->amount_of_units_in_one_box;
        return $unitsInOneBox ? $this->smallest_unit_count / $unitsInOneBox : 0;
    }
}
