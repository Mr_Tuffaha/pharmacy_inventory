<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PrescriptionContent extends Model
{
    use HasFactory;

    public $timestamps = FALSE;

    protected $fillable = [
        'medicine_id',
        'smallest_unit_count',
    ];

    protected $with = [
        'medicine'
    ];

    protected $hidden = [
        'smallest_unit_count'
    ];

    protected $appends = [
        'amount_in_boxes'
    ];

    public function medicine()
    {
        return $this->belongsTo(Medicine::class);
    }

    public function getAmountInBoxesAttribute()
    {
        return $this->medicine?->amount_of_units_in_one_box ? $this->smallest_unit_count / $this->medicine->amount_of_units_in_one_box : 0;
    }
}
