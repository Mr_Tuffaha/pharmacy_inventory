<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    public $timestamps = FALSE;

    protected $fillable = [
        'name',
        'access_level',
    ];

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
