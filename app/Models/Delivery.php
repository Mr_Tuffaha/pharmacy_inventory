<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    use HasFactory;

    protected $with = [
        'content',
        'reciever'
    ];

    public function content()
    {
        return $this->hasMany(DeliveryContent::class);
    }

    public function reciever()
    {
        return $this->belongsTo(User::class, 'recipient_user_id');
    }
}
