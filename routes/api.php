<?php

use App\Http\Controllers\API\DeliveryContentController;
use App\Http\Controllers\API\PrescriptionContentController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->name('api.')->group(function () {

    Route::get('/prescriptions/content', [PrescriptionContentController::class, 'index'])->name('prescriptions.content');
    Route::get('/deliveries/content', [DeliveryContentController::class, 'index'])->name('deliveries.content');
    Route::get('/user', function (Request $request) {
        return $request->user();
    });
});
