<!-- Modal -->
<div class="modal {{$canFade ?? false ? 'fade':''}}" id="{{$modalId}}" tabindex="-1" role="dialog" aria-labelledby="{{$modalId}}Title" aria-hidden="true">
    <div class="modal-dialog {{$modalLarge ?? false ? "modal-lg":""}} modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="{{$modalId}}Title">{{$modalTitle}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{$modalBody}}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
