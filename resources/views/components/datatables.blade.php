<table class="table table-bordered" id="{{$tableId}}" width="100%" cellspacing="0">
    <thead>
        {{$thead}}
    </thead>
    <tfoot>
        {{$tfoot}}
    </tfoot>
    <tbody>
        {{$tbody}}
    </tbody>
</table>
