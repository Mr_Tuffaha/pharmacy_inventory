<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="stylesheet" href="{{ asset('css/sb-admin.css') }}">

        <title>{{ config('app.name', 'Laravel') }}</title>
    </head>
    <body class="sb-nav-fixed">

        @include('components.sb-admin.topbar')

        <div id="layoutSidenav">

            @include('components.sb-admin.sidenav')
            <div id="layoutSidenav_content">
                <main>
                    {{ $slot }}
                </main>

                @include('components.sb-admin.footer')
            </div>
        </div>
        <script src="{{asset('js/sb-admin.js')}}"></script>
        {{ $scripts }}
    </body>
</html>
