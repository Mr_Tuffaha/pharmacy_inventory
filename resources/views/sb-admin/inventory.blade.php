<x-admin-layout>
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <h1 class="mt-4">Tables</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
            <li class="breadcrumb-item active">Inventory</li>
        </ol>
        <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-table mr-1"></i>
                Inventory in <?=$locationName?>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Type</th>
                                <th>Boxes available</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Name</th>
                                <th>Type</th>
                                <th>Boxes available</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach ($inventory as $inventoryItem)
                            <tr>
                                <td><?=$inventoryItem->medicine->name?></td>
                                <td><?=$inventoryItem->medicine->type->name?></td>
                                <td><?=$inventoryItem->amount_in_boxes?></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <x-slot name="scripts">
        <script>
            $(document).ready(function() {
                $('#dataTable').DataTable();
            });
        </script>

    </x-slot>
</x-admin-layout>
