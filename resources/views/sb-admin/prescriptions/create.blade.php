<x-admin-layout>
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <h1 class="mt-4">Create Prescription</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{route('prescriptions.index')}}">Prescriptions</a></li>
            <li class="breadcrumb-item active">Create</li>
        </ol>
        <form action="{{route('prescriptions.create')}}" id="prescriptionForm" method="post" onsubmit="return validateForm()">
            @csrf
            <div class="form-group">
                <label for="id_on_paper">Id on Paper:</label>
                <input type="text" class="form-control" id="id_on_paper" placeholder="Enter Id on Paper" name="id_on_paper" required>
            </div>
            <div class="form-group">
                <label for="patient_id">Patient Id:</label>
                <input type="text" class="form-control" id="patient_id" placeholder="Enter patient id" name="patient_id" required>
            </div>
            <div class="form-group form-inline">
                <label for="patient_type">Patient Type:</label>
                <select class="form-control" id="patient_type" name="patient_type" required>
                @foreach ($patientTypes as $type)
                    <option value="{{$type->id}}">{{$type->name}}</option>
                @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="note">Note: </label>
                <textarea class="form-control" id="note" name="note" rows="3"></textarea>
            </div>
            <div id="prescription-content">
                <div class="form-group form-inline">
                    <label for="">Medicine:
                        <div>
                            <select class="form-control" id="" name="prescriptionContent[0][medicine_id]" onblur="isMedicineDuplicateFound()" required>
                            @foreach ($invetoryMedicines as $inventoryItem)
                                <option value="{{$inventoryItem['medicine_id']}}">{{$inventoryItem['medicine_name']}}</option>
                            @endforeach
                            </select>
                            <div class="invalid-feedback"></div>
                        </div>
                    </label>
                    <label for="">Amount:
                        <div>
                            <input type="number" class="form-control" id="" name="prescriptionContent[0][amountInBoxes]" value="0" pattern="[0-9]+""  onblur="isAmountRequestedValid(this)" placeholder="Amount in boxes" required>
                            <div class="invalid-feedback"></div>
                        </div>
                    </label>
                    <i class="fas fa-plus-circle" onclick="appendForm()"></i>
                </div>
            </div>
            <div class="form-group">
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>


        </form>

    </div>

    <x-slot name="scripts">
        <script>
            var inventoryData = {!!json_encode($invetoryMedicines)!!};
            var counter = 1;

            function validateForm(){
                formIsValid = true;

                if (isMedicineDuplicateFound()) formIsValid = false;

                $('#prescription-content input').each(function(key, selectInput) {
                    if(!isAmountRequestedValid(selectInput)) formIsValid = false;
                });
                return formIsValid;
            }

            function isMedicineDuplicateFound() {
                console.log('test');
                tempMedicineMap = [];
                foundError = false;
                $('#prescription-content select').each(function(key, selectInput){

                    if(typeof tempMedicineMap[selectInput.value] === 'undefined') {
                        tempMedicineMap[selectInput.value] = 1;
                        unSetInputInvalid(selectInput);
                    }
                    else {
                        tempMedicineMap[selectInput.value]++;
                        console.log(tempMedicineMap[selectInput.value]);
                        if (tempMedicineMap[selectInput.value] > 1) {
                            setInputInvalid(selectInput, 'Medicine is already in the list');
                            foundError = true;
                        } else {
                            unSetInputInvalid(selectInput);
                        }
                    }
                });
                return foundError;
            }

            function appendForm(){
                $('#prescription-content').append(
                `
                <div class="form-group form-inline">
                    <label for="">Medicine:
                        <div>
                            <select class="form-control" id="" name="prescriptionContent[`+counter+`][medicine_id]" onblur="isMedicineDuplicateFound()" required>
                            @foreach ($invetoryMedicines as $inventoryItem)
                                <option value="{{$inventoryItem['medicine_id']}}">{{$inventoryItem['medicine_name']}}</option>
                            @endforeach
                            </select>
                            <div class="invalid-feedback"></div>
                        </div>
                    </label>
                    <label for="">Amount:
                        <div>
                            <input type="number" class="form-control" id="" name="prescriptionContent[`+counter+`][amountInBoxes]" value="0" pattern="[0-9]+""  onblur="isAmountRequestedValid(this)" placeholder="Amount in boxes" required>
                            <div class="invalid-feedback"></div>
                        </div>
                    </label>
                    <i class="fas fa-plus-circle" onclick="appendForm()"></i>
                    <i class="fas fa-minus-circle" onclick="removeFromForm(this)"></i>
                </div>
                `
                );

                counter++;
            }

            function removeFromForm(prescriptionContentGroup){
                $(prescriptionContentGroup).parent().remove();
            }

            function isAmountRequestedValid(input) {
                inputValue = parseInt(input.value == '' ? 0 : input.value);
                medicineId = $(input).parent().parent().parent().find('select')[0].value;
                medicineAmountAvailable = inventoryData[medicineId]['medicine_amount_in_boxes'];
                if(inputValue === NaN || inputValue <= 0){
                    setInputInvalid(input, 'Invalid Number');
                    return false;
                }else if( medicineAmountAvailable < inputValue ) {
                    setInputInvalid(input, 'Number bigger than available amount ('+medicineAmountAvailable+')');
                    return false;
                }else{
                    unSetInputInvalid(input);
                    return true;
                }
            }

            function setInputInvalid(input, msg) {

                $(input).addClass('is-invalid');
                $(input).parent().find('.invalid-feedback').first().text(msg);
            }

            function unSetInputInvalid(input) {
                $(input).removeClass('is-invalid');
            }

        </script>
    </x-slot>
</x-admin-layout>
