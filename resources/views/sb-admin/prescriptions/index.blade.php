<x-admin-layout>
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <h1 class="mt-4">Tables</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
            <li class="breadcrumb-item active">Prescriptions</li>
        </ol>
        <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-table mr-1"></i>
                Prescriptions in <?=$locationName?>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <x-datatables tableId="prescriptionsTable">
                        <x-slot name="thead">
                            <tr>
                                <th></th>
                                <th>Id on Paper</th>
                                <th>Patient Id</th>
                                <th>Patient Type</th>
                                <th>Prescriber</th>
                                <th>Options</th>
                            </tr>
                        </x-slot>
                        <x-slot name="tfoot">
                            <tr>
                                <th></th>
                                <th>Id on Paper</th>
                                <th>Patient Id</th>
                                <th>Patient Type</th>
                                <th>Prescriber</th>
                                <th>Options</th>
                            </tr>
                        </x-slot>
                        <x-slot name="tbody">
                            @foreach ($prescriptions as $prescriptionItem)
                            <tr>
                                <td class="details-control" style="text-align:center">
                                    <i class="fas fa-plus-circle"></i>
                                    <div style="display: none;">{{$prescriptionItem->id}}</div>
                                </td>
                                <td>{{$prescriptionItem->id_on_paper}}</td>
                                <td>{{$prescriptionItem->patient_id}}</td>
                                <td>{{$prescriptionItem->patientType->name}}</td>
                                <td>{{$prescriptionItem->prescriber->firstname.' '.$prescriptionItem->prescriber->lastname}}</td>
                                <td>
                                    @if ($prescriptionItem->note)
                                    <button type="button" class="btn btn-primary prescription-note">View Note
                                        <div style="display: none">
                                            {{$prescriptionItem->note}}
                                        </div>
                                    </button>
                                    @else
                                         No Note
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </x-slot>
                    </x-datatables>
                </div>
            </div>
        </div>
    </div>

    <x-modal :canFade=true modalId="prescriptionContentModal" :modalLarge=true>
        <x-slot name="modalTitle">
            Prescription Content
        </x-slot>
        <x-slot name="modalBody">
            <x-datatables tableId="prescriptionContentTable">
                <x-slot name="thead">
                    <tr>
                        <th>Medicine</th>
                        <th>Type</th>
                        <th>Amount</th>
                    </tr>
                </x-slot>
                <x-slot name="tfoot">
                    <tr>
                        <th>Medicine</th>
                        <th>Type</th>
                        <th>Amount</th>
                    </tr>
                </x-slot>
                <x-slot name="tbody">

                </x-slot>
            </x-datatables>
        </x-slot>
    </x-modal>

    <x-modal modalId="noteModal" :modalLarge=false>
        <x-slot name="modalTitle">
            Prescription Note:
        </x-slot>
        <x-slot name="modalBody">

        </x-slot>
    </x-modal>


    <x-modal modalId="loadingModal" :modalLarge=false>
        <x-slot name="modalTitle">
            Loading ...
        </x-slot>
        <x-slot name="modalBody">
            <div style="text-align: center; font-size: 10rem; color: #007bff">
                <i class="fas fa-spinner fa-spin"></i>
            </div>
        </x-slot>
    </x-modal>

    <x-modal modalId="errorModal" :modalLarge=false>
        <x-slot name="modalTitle">
            Something happened!!!
        </x-slot>
        <x-slot name="modalBody">
            <div style="text-align: center; font-size: 10rem; color: #ff0000">
                <i class="fas fa-times-circle"></i>
            </div>
        </x-slot>
    </x-modal>

    <x-slot name="scripts">
        <script>
            $(document).ready(function() {
                $('#prescriptionsTable').DataTable();

                $('#prescriptionsTable > tbody').on('click', 'td.details-control', function () {
                    var prescriptionId = $(this).closest('td').find('div').text();
                    $.ajax("{{route('api.prescriptions.content')}}",{
                        method: "GET",
                        data: {"prescription_id": prescriptionId},
                        beforeSend: function() {
                            $('#loadingModal').modal();
                        }
                    }).always(function(){
                        $('#loadingModal').modal('hide');
                    }).fail(function(){
                        $('#errorModal').modal();
                    }).done(function(data) {
                        var prescriptionContentTableHtml = "";
                        $.each(data,function(key,row){
                            prescriptionContentTableHtml+=`
                            <tr>
                                <td>`+row.medicine.name+`</td>
                                <td>`+row.medicine.type.name+`</td>
                                <td>`+row.amount_in_boxes+`</td>
                            </tr>
                            `;
                        });

                        $('#prescriptionContentTable tbody').html(prescriptionContentTableHtml);

                        $('#prescriptionContentModal').modal();
                    });
                });
            });

            $('#prescriptionsTable > tbody').on('click', 'button', function () {
                var note = $(this).children().first().text();
                $('#noteModal div.modal-body').html(note.trim());
                $('#noteModal').modal();
            });


        </script>

    </x-slot>
</x-admin-layout>
