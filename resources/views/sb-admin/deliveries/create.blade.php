<x-admin-layout>
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <h1 class="mt-4">Create Delivery</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{route('deliveries.index')}}">Deliveries</a></li>
            <li class="breadcrumb-item active">Create</li>
        </ol>
        <form action="{{route('deliveries.create')}}" id="deliveryForm" method="post" onsubmit="return validateForm()">
            @csrf
            <div class="form-group">
                <label for="id_in_receipt">Id on Receipt:</label>
                <input type="text" class="form-control" id="id_in_receipt" placeholder="Enter Id on receipt" name="id_in_receipt" required>
            </div>
            <div class="form-group">
                <label for="id_in_notebook">Id in Notebook:</label>
                <input type="text" class="form-control" id="id_in_notebook" placeholder="Enter Id on notebook" name="id_in_notebook" required>
            </div>
            <div class="form-group">
                <label for="deliverer">Deliverer:</label>
                <input type="text" class="form-control" id="deliverer" placeholder="Enter deliverer" name="deliverer" required>
            </div>
            <div id="delivery-content">
                <div class="form-group form-inline">
                    <label for="">Medicine:
                        <div>
                            <select class="form-control" id="" name="deliveryContent[0][medicine_id]" onblur="isMedicineDuplicateFound()" required>
                            @foreach ($medicines as $medicine)
                                <option value="{{$medicine->id}}">{{$medicine->name}}</option>
                            @endforeach
                            </select>
                            <div class="invalid-feedback"></div>
                        </div>
                    </label>
                    <label for="">Amount:
                        <div>
                            <input type="number" class="form-control" id="" name="deliveryContent[0][amountInBoxes]" value="0" pattern="[0-9]+""  onblur="isAmountRequestedValid(this)" placeholder="Amount in boxes" required>
                            <div class="invalid-feedback"></div>
                        </div>
                    </label>
                    <i class="fas fa-plus-circle" onclick="appendForm()"></i>
                </div>
            </div>
            <div class="form-group">
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>


        </form>

    </div>

    <x-slot name="scripts">
        <script>
            var counter = 1;

            function validateForm(){
                formIsValid = true;

                if (isMedicineDuplicateFound()) formIsValid = false;

                $('#delivery-content input').each(function(key, selectInput) {
                    if(!isAmountRequestedValid(selectInput)) formIsValid = false;
                });
                return formIsValid;
            }

            function isMedicineDuplicateFound() {
                console.log('test');
                tempMedicineMap = [];
                foundError = false;
                $('#delivery-content select').each(function(key, selectInput){

                    if(typeof tempMedicineMap[selectInput.value] === 'undefined') {
                        tempMedicineMap[selectInput.value] = 1;
                        unSetInputInvalid(selectInput);
                    }
                    else {
                        tempMedicineMap[selectInput.value]++;
                        console.log(tempMedicineMap[selectInput.value]);
                        if (tempMedicineMap[selectInput.value] > 1) {
                            setInputInvalid(selectInput, 'Medicine is already in the list');
                            foundError = true;
                        } else {
                            unSetInputInvalid(selectInput);
                        }
                    }
                });
                return foundError;
            }

            function appendForm(){
                $('#delivery-content').append(
                `
                <div class="form-group form-inline">
                    <label for="">Medicine:
                        <div>
                            <select class="form-control" id="" name="deliveryContent[`+counter+`][medicine_id]" onblur="isMedicineDuplicateFound()" required>
                            @foreach ($medicines as $medicine)
                                <option value="{{$medicine->id}}">{{$medicine->name}}</option>
                            @endforeach
                            </select>
                            <div class="invalid-feedback"></div>
                        </div>
                    </label>
                    <label for="">Amount:
                        <div>
                            <input type="number" class="form-control" id="" name="deliveryContent[`+counter+`][amountInBoxes]" value="0" pattern="[0-9]+""  onblur="isAmountRequestedValid(this)" placeholder="Amount in boxes" required>
                            <div class="invalid-feedback"></div>
                        </div>
                    </label>
                    <i class="fas fa-plus-circle" onclick="appendForm()"></i>
                    <i class="fas fa-minus-circle" onclick="removeFromForm(this)"></i>
                </div>
                `
                );

                counter++;
            }

            function removeFromForm(deliveryContentGroup){
                $(deliveryContentGroup).parent().remove();
            }

            function isAmountRequestedValid(input) {
                inputValue = parseInt(input.value == '' ? 0 : input.value);
                if(inputValue === NaN || inputValue <= 0){
                    setInputInvalid(input, 'Invalid Number');
                    return false;
                }else{
                    unSetInputInvalid(input);
                    return true;
                }
            }

            function setInputInvalid(input, msg) {

                $(input).addClass('is-invalid');
                $(input).parent().find('.invalid-feedback').first().text(msg);
            }

            function unSetInputInvalid(input) {
                $(input).removeClass('is-invalid');
            }

        </script>
    </x-slot>
</x-admin-layout>
