<x-admin-layout>
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <h1 class="mt-4">Tables</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
            <li class="breadcrumb-item active">Deliveries</li>
        </ol>
        <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-table mr-1"></i>
                Deliveries in <?=$locationName?>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <x-datatables tableId="deliveriesTable">
                        <x-slot name="thead">
                            <tr>
                                <th></th>
                                <th>Id in receipt</th>
                                <th>Id in notebook</th>
                                <th>Delvierer</th>
                                <th>Reciever</th>
                                <th>Datetime</th>
                            </tr>
                        </x-slot>
                        <x-slot name="tfoot">
                            <tr>
                                <th></th>
                                <th>Id in receipt</th>
                                <th>Id in notebook</th>
                                <th>Delvierer</th>
                                <th>Reciever</th>
                                <th>Datetime</th>
                            </tr>
                        </x-slot>
                        <x-slot name="tbody">
                            @foreach ($deliveries as $deliveryItem)
                            <tr>
                                <td class="details-control" style="text-align:center">
                                    <i class="fas fa-plus-circle"></i>
                                    <div style="display: none;">{{$deliveryItem->id}}</div>
                                </td>
                                <td>{{$deliveryItem->id_in_receipt}}</td>
                                <td>{{$deliveryItem->id_in_notebook}}</td>
                                <td>{{$deliveryItem->deliverer}}</td>
                                <td>{{$deliveryItem->reciever->firstname.' '.$deliveryItem->reciever->lastname}}</td>
                                <td>{{$deliveryItem->created_at}}</td>
                            </tr>
                            @endforeach
                        </x-slot>
                    </x-datatables>
                </div>
            </div>
        </div>
    </div>

    <x-modal :canFade=true modalId="deliveryContentModal" :modalLarge=true>
        <x-slot name="modalTitle">
            Delivery Content
        </x-slot>
        <x-slot name="modalBody">
            <x-datatables tableId="deliveryContentTable">
                <x-slot name="thead">
                    <tr>
                        <th>Medicine</th>
                        <th>Type</th>
                        <th>Amount</th>
                    </tr>
                </x-slot>
                <x-slot name="tfoot">
                    <tr>
                        <th>Medicine</th>
                        <th>Type</th>
                        <th>Amount</th>
                    </tr>
                </x-slot>
                <x-slot name="tbody">

                </x-slot>
            </x-datatables>
        </x-slot>
    </x-modal>


    <x-modal modalId="loadingModal" :modalLarge=false>
        <x-slot name="modalTitle">
            Loading ...
        </x-slot>
        <x-slot name="modalBody">
            <div style="text-align: center; font-size: 10rem; color: #007bff">
                <i class="fas fa-spinner fa-spin"></i>
            </div>
        </x-slot>
    </x-modal>

    <x-modal modalId="errorModal" :modalLarge=false>
        <x-slot name="modalTitle">
            Something happened!!!
        </x-slot>
        <x-slot name="modalBody">
            <div style="text-align: center; font-size: 10rem; color: #ff0000">
                <i class="fas fa-times-circle"></i>
            </div>
        </x-slot>
    </x-modal>

    <x-slot name="scripts">
        <script>
            $(document).ready(function() {
                $('#deliveriesTable').DataTable();

                $('#deliveriesTable > tbody').on('click', 'td.details-control', function () {
                    var deliveryId = $(this).closest('td').find('div').text();
                    $.ajax("{{route('api.deliveries.content')}}",{
                        method: "GET",
                        data: {"delivery_id": deliveryId},
                        beforeSend: function() {
                            $('#loadingModal').modal();
                        }
                    }).always(function(){
                        $('#loadingModal').modal('hide');
                    }).fail(function(){
                        $('#errorModal').modal();
                    }).done(function(data) {
                        var deliveryContentTableHtml = "";
                        $.each(data,function(key,row){
                            deliveryContentTableHtml+=`
                            <tr>
                                <td>`+row.medicine.name+`</td>
                                <td>`+row.medicine.type.name+`</td>
                                <td>`+row.amount_in_boxes+`</td>
                            </tr>
                            `;
                        });

                        $('#deliveryContentTable tbody').html(deliveryContentTableHtml);

                        $('#deliveryContentModal').modal();
                    });
                });
            });


        </script>

    </x-slot>
</x-admin-layout>
