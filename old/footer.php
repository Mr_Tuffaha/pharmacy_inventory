<footer class="body-footer">
    <p id="copyright" property="dc:rights">&copy;
      <span property="dc:dateCopyrighted"><?php echo date("Y");?></span>
      <span property="dc:publisher">German Jordanian University</span>
    </p>
</footer>
