</div>
<script>
var enctRole = "<?php echo $encRole; ?>";
function showMsg(msg,type) {
    var error = '<div class="error-msg msg"><i class="fa fa-times-circle"></i>'+msg+'</div>';
    var warning = '<div class="warning-msg msg"><i class="fa fa-warning"></i>'+msg+'</div>';
    var success = '<div class="success-msg msg"><i class="fa fa-check"></i>'+msg+'</div>';
    var info = '<div class="info-msg msg"><i class="fa fa-info-circle"></i>'+msg+'</div>';
    switch (type) {
        case 'error':
        return error;
        break;
        case 'warning':
        return warning;
        break;
        case 'success':
        return success;
        break;
        case 'info':
        return info;
        break;
        default:
    }
}

</script>
<?php
// echo "<script src='https://code.jquery.com/jquery-3.3.1.min.js' integrity='sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=' crossorigin='anonymous'></script>\n";
echo "<script src='js/jquery-3.3.1.min.js'></script>\n";

$jquery_ui = "<script src='js/jquery-ui.js'></script>\n";
$datatables = "<script src='js/datatables.min.js'></script>\n";
$full_datatables = "<script src='assets/datatables/datatables.min.js'></script>\n";
$pdfmake = "<script src='js/pdfmake.min.js'></script>\n";
$vfs_fonts = "<script src='js/vfs_fonts.js'></script>\n";
switch ($pageName) {
    case 'profile':
    echo "<script src='js/profile.js'></script>\n";
    break;
    case 'inventory':
    // echo "<script src='https://code.jquery.com/ui/1.12.1/jquery-ui.js' integrity='sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30=' crossorigin='anonymous'></script>\n";
    // echo "<script src='js/jquery-ui.js'></script>\n";
    echo $jquery_ui;
    // echo "<script src='https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js'></script>\n";
    // echo "<script src='js/pdfmake.min.js'></script>\n";
    // echo $pdfmake;
    // echo "<script src='https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js'></script>\n";
    // echo "<script src='js/vfs_fonts.js'></script>\n";
    // echo $vfs_fonts;
    // echo "<script src='https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/b-1.5.1/b-colvis-1.5.1/b-html5-1.5.1/b-print-1.5.1/datatables.min.js'></script>\n";
    // echo "<script src='js/datatables.min.js'></script>\n";
    // echo $datatables;
    echo $full_datatables;
    echo "<script src='js/index.js'></script>";
    // echo "<script src='js/modal_edit_inventory.js'></script>";
    break;
    case 'delivery':
    // echo "<script src='https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js'></script>\n";
    // echo "<script src='js/pdfmake.min.js'></script>\n";
    // echo $pdfmake;
    // echo "<script src='https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js'></script>\n";
    // echo "<script src='js/vfs_fonts.js'></script>\n";
    // echo $vfs_fonts;
    // echo "<script src='https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/b-1.5.1/b-colvis-1.5.1/b-html5-1.5.1/b-print-1.5.1/datatables.min.js'></script>\n";
    // echo "<script src='js/datatables.min.js'></script>\n";
    // echo $datatables;
    echo $full_datatables;
    echo "<script src='js/delivery.js'></script>";
    break;
    case 'perscription':
    // echo "<script src='js/pdfmake.min.js'></script>\n";
    // echo $pdfmake;
    // echo "<script src='https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js'></script>\n";
    // echo "<script src='js/vfs_fonts.js'></script>\n";
    // echo $vfs_fonts;
    // echo "<script src='https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js'></script>\n";
    // echo "<script src='js/datatables.min.js'></script>\n";
    // echo $datatables;
    echo $full_datatables;
    // echo "<script src='https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/b-1.5.1/b-colvis-1.5.1/b-html5-1.5.1/b-print-1.5.1/datatables.min.js'></script>\n";
    echo "<script src='js/perscription.js'></script>";
    break;
    case 'add_delivery':
    case 'add_perscription':
    case 'request_delivery':
    case 'recieve_delivery':
    case 'send_delivery':
    echo "<script src='js/add_script.js'></script>\n";
    break;

    default:
    # code...
    break;
}


switch ($pageName) {
    case 'inventory':
    case 'add_delivery':
    echo "<script src='js/modal.js'></script>\n";
    break;
    default:
    # code...
    break;
}

?>

</body>
</html>
