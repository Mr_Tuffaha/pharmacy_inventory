<?php

include_once __DIR__.'/includes/SessionManager.php';
SessionManager::sessionStart($sessionName, $lifetime, $cookiePath, $currentDomain,$https);

session_unset();
session_destroy();
if(isset($_COOKIE['autologin'])){
    include_once __DIR__.'/includes/Autologin.php';
    Autologin::deleteToken();
}
header("location: login.php");
