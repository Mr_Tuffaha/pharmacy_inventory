<?php
include_once 'php_head.php';
$pageName = "inventory";
$title = "Pharmacy Inventory";
$dicription = "";
if(isset($_POST['enct'])){
    if($_POST['enct']&&$_POST['medicine-name']&&$_POST['medicine-type']&&$_POST['num-of-sachet']&&$_POST['num-of-pills']){
        include_once __DIR__.'/includes/Inventory.php';
        $inventory = new Inventory();
        $inventory->insertNewMedicine($_POST['medicine-name'],$_POST['medicine-type'],$_POST['num-of-sachet'],$_POST['num-of-pills']);
        //header('location: index.php');
    }
}
include_once __DIR__.'/header.php';
include_once __DIR__.'/aside.php';
include_once __DIR__.'/modal/modal.php';
include_once __DIR__.'/modal/inventory_edit_modal.php';
include_once __DIR__.'/modal/notice-modal.php';
?>


<main class="body-main">
        <h1>Inventory</h1>
        <div class="divider"></div>
        <!-- Trigger/Open The Modal -->
        <table id="myTable" class="display .compact .nowrap">

        </table>
        <button id="add-medicine-btn" class="add-medicine-btn btn">Add New Medicine</button>
</main>

<?php
include_once 'footer.php';
include_once 'scripts_and_end_page.php';
?>
