<aside class="body-aside">
    <img src="img/<?php echo isset($_SESSION['user_image'])?$_SESSION['user_image']:'profile.png';?>" class="profile-image" alt="Profile Image">
    <h3 class="username"><?php echo isset($_SESSION['user_role'])?$_SESSION['user_firstname'].' '.$_SESSION['user_lastname']:'';?></h3>
    <div class="divider"></div>
    <div class="menu-link-container">
        <a class="menu-link" href="index.php">Inventory</a>
        <?php if(isset($_SESSION['user_role'])&&$_SESSION['user_role']=='Madaba'){ ?>
            <a class="menu-link" href="delivery.php">See deliveries</a>
            <a class="menu-link" href="add_delivery.php">Add delivery</a>
        <?php } ?>

        <?php if(isset($_SESSION['user_role'])&&$_SESSION['user_role']=='Madaba'&&$currentStage==1){ ?>
            <a class="menu-link" href="send_delivery.php">Send delivery</a>
        <?php } ?>

        <?php if(isset($_SESSION['user_role'])&&$_SESSION['user_role']=='Jabal Amman'&&$currentStage==0){ ?>
            <a class="menu-link" href="request_delivery.php">Request delivery</a>
        <?php } ?>
        <?php if(isset($_SESSION['user_role'])&&$_SESSION['user_role']=='Jabal Amman'&&$currentStage==2){ ?>
            <a class="menu-link" href="recieve_delivery.php">Recieve delivery</a>
        <?php } ?>
        <a class="menu-link" href="perscription.php">See perscriptions</a>
        <a class="menu-link" href="add_perscription.php">Add perscription</a>
        <a class="menu-link" href="profile.php">Profile</a>
        <a class="menu-link" href="signout.php">Sign out</a>
    </div>
</aside>
