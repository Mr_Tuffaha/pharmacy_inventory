<?php
include_once 'php_head.php';
$pageName = "add_perscription";
$title = "Pharmacy Perscription";
$dicription = "";
if(isset($_POST['perscription_id'])){
    include_once __DIR__.'/includes/Perscription.php';
    $perscription = new Perscription();
    if(!$perscription->checkExistIdInLocation($_POST['perscription_id'],$_SESSION['user_role'])){
        $content = array();
        $length = sizeof($_POST['medicine']);
        for ($i=0; $i < $length; $i++) {
            $content[] = array($_POST['medicine'][$i],$_POST['quantity'][$i]);
        }
        $perscription->insertPerscription($_POST['perscription_id'],$_SESSION['user_id'],$_POST['student_employe_id'],$_POST['patiant_type'],$_SESSION['user_role'],$content,$_POST['perscription_note']);

    }else{

    }
}






include_once 'header.php';
include_once 'aside.php';
?>
<main class="body-main" id="body-main">
    <!-- <p class="error-msg notify"><i class="fas fa-exclamation-circle"></i>Perscription id already exists</p> -->
    <h1>Add Perscription</h1>
    <div class="divider"></div>
    <form class="add-form" id="add-perscription-form" action="add_perscription.php" method="post">
        <div class="form-element" id="perscription_id_fields">
            <label for="">Perscription #:</label>
            <input type="text" name="perscription_id" value="" placeholder="ID">
        </div>
        <div class="form-element">
            <label for="">Student/Employe #:</label>
            <input type="text" class="optional" name="student_employe_id" value="" placeholder="ID">
        </div>
        <div class="form-element">
            <label for="">Type:</label>
            <select name="patiant_type">
                <option value="student">Student</option>
                <option value="employee">Employee</option>
            </select>
        </div>
        <div class="form-element textarea">
            <label for="">Note:</label>
            <textarea name="perscription_note" rows="4" cols="80"></textarea>
        </div>
        <fieldset>
            <legend>List</legend>
            <div class="form-element list">
                <label for="">Medicine:</label>
                <select class="medicine-select" name="medicine[]">
                </select>
                <input type="text" name="quantity[]" value="" placeholder="number of pills Ex:0">
                <!-- <input type="text" name="quantity_sachet[]" value="" placeholder="number of sachets Ex:0"> -->
                <!-- <input type="text" name="quantity_pills[]" value="" placeholder="number of pills Ex:0"> -->
            </div>
        </fieldset>
    </form>
    <div class="form-element">
        <button type="button" class="green-btn btn" id="add-element" name="add">Add more</button>
        <button type="button" class="submit-btn btn" id="submit-btn" name="submitbtn">Submit</button>
    </div>

</main>

<?php
include_once 'footer.php';
include_once 'scripts_and_end_page.php';
?>
