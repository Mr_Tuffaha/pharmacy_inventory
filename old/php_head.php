<?php
include_once __DIR__.'/includes/SessionManager.php';
if(!$mode){
    header("location: fixing.php");
    die();
}
SessionManager::sessionStart($sessionName, $lifetime, $cookiePath, $currentDomain,$https);

if (!isset($_SESSION['user_id'])) {
    header("location: login.php");
    die();
}else{
    $encRole = password_hash($_SESSION['user_role'],PASSWORD_DEFAULT);
    // $token = password_hash($_SESSION['user_role'].(time()/300)%(60*24),PASSWORD_DEFAULT);
    // echo $encRole;
    include_once __DIR__ . '/includes/InnerDelivery.php';
    $innerDelivery = new InnerDelivery();
    $currentStage = $innerDelivery->checkCurrentStage();
}
 ?>
