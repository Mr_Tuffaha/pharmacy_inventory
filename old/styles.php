
<link href="https://fonts.googleapis.com/css?family=IBM+Plex+Mono:400,700" rel="stylesheet">
<!-- <link rel="stylesheet" href="css/fonts.css"> -->
<?php

$jquery_ui = "<link rel='stylesheet' href='css/jquery_ui.css'>\n";
$datatables = "<link rel='stylesheet' href='assets/datatables/datatables.min.css'>\n";
$fontawsome = "<link rel='stylesheet' href='css/fontawesome-free-5.0.9/web-fonts-with-css/css/fontawesome-all.min.css'>\n";
switch ($pageName) {
    case 'perscription':
    case 'delivery':
    case 'inventory':
    echo $jquery_ui;
    echo $datatables;
    echo $fontawsome;
    echo "<link rel='stylesheet' href='css/notice_modal.css'>\n";
    echo "<link rel='stylesheet' href='css/edit_modal.css'>\n";
        break;
    case 'add_perscription':
    case 'add_delivery':
    case 'request_delivery':
    case 'recieve_delivery':
    case 'send_delivery':
    case 'profile':
    echo $fontawsome;
        break;
    default:
        # code...
        break;
}
switch ($pageName) {
    case 'inventory':
    case 'add_delivery':
    echo "<link rel='stylesheet' href='css/add_modal.css'>\n";
        break;
    default:
    # code...
        break;
}
?>
<link rel='stylesheet' href='css/style.css'>
