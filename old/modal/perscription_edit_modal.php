<!-- The Modal -->
<div id="edit-perscription-modal" class="edit-modal">

    <!-- Modal content -->
    <div id="edit-perscription-modal-content" class="modal-content">
        <div class="modal-header">
            <!-- <span class="close">&times;</span> -->
            <h2>Edit Medicine</h2>
        </div>
        <div class="modal-body">
            <div class="edit">
                <form class="edit-form" id="edit-perscription-form" action="" method="post">
                    <div class="form-element" id="perscription_id_fields">
                        <label for="">Perscription #:</label>
                        <input type="text" name="perscription_id" value="" placeholder="ID">
                    </div>
                    <div class="form-element">
                        <label for="">Student/Employe #:</label>
                        <input type="text" class="optional" name="student_employe_id" value="" placeholder="ID">
                    </div>
                    <div class="form-element list">
                        <label for="">Type:</label>
                        <select name="patiant_type">
                            <option value="student">Student</option>
                            <option value="employee">Employee</option>
                        </select>
                    </div>
                    <fieldset>
                        <legend>List</legend>
                        <div class="form-element list">
                            <label for="">Medicine:</label>
                            <select class="medicine-select" name="medicine[]">
                            </select>
                            <input type="text" name="quantity[]" value="" placeholder="number of pills Ex:0">
                        </div>
                    </fieldset>
                </form>
                <div class="form-element">
                    <button type="button" class="green-btn btn" id="add-element" name="add">Add more</button>
                </div>
            </div>
            <div class="loading">
                <i class="fas fa-circle-notch"></i>
                <p>loading ...</p>
            </div>
            <div class="done">
                <i class="far fa-check-circle"></i>
                <p>Done</p>
            </div>
        </div>
        <div class="modal-footer">
            <div class="edit">
                <button type="button" class="green-btn btn" id="edit-perscription-btn" name="edit-perscription-submit">Edit Medicine</button>
                <button type="button" class="cancle-btn btn" id="edit-perscription-cancle-btn" name="edit-perscription-cancle-btn">Cancle</button>
            </div>
            <div class="done">
                <button type="button" class="green-btn btn" id="edit-perscription-close-btn" name="">Close</button>
            </div>
        </div>
    </div>

</div>
