<!-- The Modal -->
<div id="delete-notice" class="modal-delete">

    <!-- Modal content -->
    <div id="delete-notice-modal-system" class="modal-content">
        <div class="modal-header">
            <div class="modal-title">
                <!-- <i class="fas fa-esxclamation-triangle"></i> -->
                <h2>Deleting</h2>
            </div>
            <div class="modal-close">
                <!-- <span class="close">&times;</span> -->
            </div>
        </div>
        <div class="modal-body">
            <div class="notice">
                <i class="fas fa-exclamation-triangle"></i>
                <p>Are you sure you want to delete?</p>
                <p class="target"></p>
            </div>
            <div class="loading">
                <i class="fas fa-circle-notch"></i>
                <p>loading ...</p>
            </div>
            <div class="done">
                <i class="far fa-check-circle"></i>
                <p>Done</p>
            </div>
        </div>
        <div class="modal-footer">
            <div class="notice">
                <button type="button" class="red-btn btn" id="delete-notice-delete-btn" name="">Delete</button>
                <button type="button" class="green-btn btn" id="delete-notice-cancle-btn" name="">Cancle</button>
            </div>
            <div class="done">
                <button type="button" class="green-btn btn" id="delete-notice-close-btn" name="">Close</button>
            </div>
        </div>
    </div>
</div>
