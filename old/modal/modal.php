<!-- The Modal -->
<div id="add-medicine-modal" class="add-modal">

    <!-- Modal content -->
    <div id="add-medicine-modal-content" class="modal-content">
        <div class="modal-header">
            <span class="close">&times;</span>
            <h2>Add new Medicine</h2>
        </div>
        <div class="modal-body">
            <form class="add-form" id="add-medicine-form" action="" method="post">
                <input type="hidden" name="enct" value="<?php echo $encRole;?>">
                <div class="form-element" id="add-medicine-name-fields">
                    <label for="">Medicine Name:</label>
                    <input type="text" name="medicine-name" value="" placeholder="Medicine Name">
                </div>
                <div class="form-element">
                    <label for="">Medicine Type:</label>
                    <select class="" name="medicine-type">
                        <option value="pill">Pills</option>
                        <option value="syrup">Syrup</option>
                        <option value="injection">Injection</option>
                        <option value="cream">Cream</option>
                        <option value="drop">Drop</option>
                        <option value="gel">Gel</option>
                        <option value="spray">Spray</option>
                        <option value="powder">Powder</option>
                        <option value="supp">Supp</option>
                    </select>
                </div>
                <div class="form-element">
                    <label for="">Number of sachets in box:</label>
                    <input type="text" name="num-of-sachet" value="" placeholder="Number of sachets in box">
                </div>
                <div class="form-element">
                    <label for="">Number of pills in sachet:</label>
                    <input type="text" name="num-of-pills" value="" placeholder="Number of pills in sachet">
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="green-btn btn" id="add-medicine-submit-btn" name="medicine-submit">Add Medicine</button>
        </div>
    </div>

</div>
