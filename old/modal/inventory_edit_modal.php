<!-- The Modal -->
<div id="edit-medicine-modal" class="edit-modal">

    <!-- Modal content -->
    <div id="edit-medicine-modal-content" class="modal-content">
        <div class="modal-header">
            <!-- <span class="close">&times;</span> -->
            <h2>Edit Medicine</h2>
        </div>
        <div class="modal-body">
            <div class="edit">
                <form class="edit-form" id="edit-medicine-form" action="" method="post">
                <input type="hidden" name="enct" value="<?php echo $encRole;?>">
                <div class="form-element" id="medicine-name-fields">
                    <label for="">Medicine Name:</label>
                    <input type="text" name="medicine-name" value="" placeholder="Medicine Name">
                </div>
                <div class="form-element">
                    <label for="">Medicine Type:</label>
                    <select class="" name="medicine-type">
                        <option value="pill">Pills</option>
                        <option value="syrup">Syrup</option>
                        <option value="injection">Injection</option>
                        <option value="cream">Cream</option>
                        <option value="drop">Drop</option>
                        <option value="gel">Gel</option>
                        <option value="spray">Spray</option>
                        <option value="powder">Powder</option>
                        <option value="supp">Supp</option>
                    </select>
                </div>
                <div class="form-element">
                    <label for="">Number of sachets in box:</label>
                    <input type="text" name="num-of-sachet" value="" placeholder="Number of sachets in box">
                </div>
                <div class="form-element">
                    <label for="">Number of pills in sachet:</label>
                    <input type="text" name="num-of-pills" value="" placeholder="Number of pills in sachet">
                </div>
                <div class="divider">
                    <input type="hidden" name="old-name" value="">
                    <input type="hidden" name="med-id" value="">
                </div>
                <div class="form-element">
                    <label for="">Available boxes:</label>
                    <input type="text" name="num-of-boxes-available" value="" placeholder="Number of pills in sachet">
                </div>
                <div class="form-element">
                    <label for="">Available sachets:</label>
                    <input type="text" name="num-of-sachets-available" value="" placeholder="Number of pills in sachet">
                </div>
                <div class="form-element">
                    <label for="">Available pills:</label>
                    <input type="text" name="num-of-pills-available" value="" placeholder="Number of pills in sachet">
                </div>
            </form>
            </div>
            <div class="loading">
                <i class="fas fa-circle-notch"></i>
                <p>loading ...</p>
            </div>
            <div class="done">
                <i class="far fa-check-circle"></i>
                <p>Done</p>
            </div>
        </div>
        <div class="modal-footer">
            <div class="edit">
                <button type="button" class="green-btn btn" id="edit-medicine-btn" name="edit-medicine-submit">Edit Medicine</button>
                <button type="button" class="cancle-btn btn" id="edit-medicine-cancle-btn" name="edit-perscription-cancle-btn">Cancle</button>
            </div>
            <div class="done">
                <button type="button" class="green-btn btn" id="edit-medicine-close-btn" name="">Close</button>
            </div>
        </div>
    </div>

</div>
