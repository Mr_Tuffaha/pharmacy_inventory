<?php
include_once 'php_head.php';
$pageName = "send_delivery";
$title = "Pharmacy Delivery";
$dicription = "";
$requestData = $innerDelivery->fetchCurrentRequest();
if(!$requestData['content']||$_SESSION['user_role']=="Jabal Amman"){
    header("location: index.php");
}
if(isset($_POST['type'])){
    if($_POST['type']=='send_delivery'){
        $content = array();
        $length = sizeof($_POST['medicine']);
        for ($i=0; $i < $length; $i++) {
            if($_POST['quantity'][$i]!=""){
                $content[] = array($_POST['medicine'][$i],$_POST['quantity'][$i]);
            }
        }
        $innerDelivery->sendDelivery($requestData['inner_delivery_id'],$_SESSION['user_id'],$content,$_POST['note']);
        header("location: index.php");
    }
}


include_once 'header.php';
include_once 'aside.php';
?>

<main class="body-main" id="body-main">
    <h1>Request Delivery</h1>
    <div class="divider"></div>
    <p class="note">Note: <?php echo $requestData['inner_delivery_request_note']?$requestData['inner_delivery_request_note']:'';?></p>
    <form class="add-form" id="request-delivery-form" action="" method="post">
        <input type="hidden" name="type" value="send_delivery">
        <div class="form-send ">
            <div class="form-send-row ">
                <label for="">Medicine Name</label>
                <label for="">Amount available</label>
                <label for="">Amount requested</label>
                <label for="">Amount to send</label>
            </div>
            <?php foreach ($requestData['content'] as $key => $value) {?>
            <div class="form-send-row ">
                <label for=""><?php echo $value['medicine_name'];?></label>
                <label for=""><?php echo $value['num_of_boxes'];?></label>
                <label for=""><?php echo $value['count'];?></label>
                <input type="hidden" name="medicine[]" value="<?php echo $value['medicine_name'];?>">
                <input type="text" name="quantity[]" value="" placeholder="number of boxes">
            </div>
        <?php } ?>
        </div>

        <fieldset>
            <legend>add drug</legend>
            <div class="form-element list">
                <label for="">Medicine:</label>
                <select class="medicine-select" name="medicine[]">
                </select>
                <input type="text" class="optional" name="quantity[]" value="" placeholder="number of boxes">
            </div>
        </fieldset>
        <div class="form-element list">
            <label for="">Note:</label>
            <textarea name="note" cols="80" placeholder="notes"></textarea>
        </div>
    </form>
    <div class="form-element">
        <button type="button" class="add-btn btn" id="add-element" name="add">Add more</button>
        <button type="button" class="submit-btn btn" id="submit-btn" name="submitbtn">Submit</button>
    </div>

</main>

<?php
include_once 'footer.php';
include_once 'scripts_and_end_page.php';
?>
