<?php
include_once __DIR__.'/../includes/dir.php';
include_once __DIR__.'/../includes/SessionManager.php';
SessionManager::sessionStart($sessionName, $lifetime, $cookiePath, $currentDomain,$https);
if (!isset($_SESSION['user_id'])) {
    header("HTTP/1.0 401");
    die();
}

if(isset($_SERVER['HTTP_REFERER'])&&parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST) == DOMAIN&&isset($_GET['role'])){
    $location = "";
    if(password_verify("Madaba",$_GET['role'])){
        $location = "Madaba";
    }elseif (password_verify("Jabal Amman",$_GET['role'])) {
        $location = "Jabal Amman";
    }else{
        header("HTTP/1.0 401");
        die();
    }
    if(isset($_GET['id'])&&isset($_GET['type'])){
        switch ($_GET['type']) {
            case 'perscription':
                include_once __DIR__.'/../includes/Perscription.php';
                $perscription = new Perscription();
                $result = $perscription->deleteSinglePerscription($_GET['id'],$location);
                header("HTTP/1.0 {$result['status']} {$result['info']}");
                break;
            case 'delivery':
                include_once __DIR__.'/../includes/Delivery.php';
                $delivery = new Delivery();
                $result = $delivery->deleteSingleDelivery($_GET['id']);
                header("HTTP/1.0 {$result['status']} {$result['info']}");
                break;
            case 'inventory':
                include_once __DIR__.'/../includes/Inventory.php';
                $inventory = new Inventory();
                $result = $inventory->deleteMedinine($_GET['id']);
                header("HTTP/1.0 {$result['status']} {$result['info']}");
                break;

            default:
                header("HTTP/1.0 400 not valid type");
                die();
            # code...
            break;
        }
        die();
    }else{

    }
}else{
    header("HTTP/1.0 401");
    die();
}
