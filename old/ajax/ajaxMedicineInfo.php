<?php
include_once __DIR__.'/../includes/dir.php';
include_once __DIR__.'/../includes/SessionManager.php';
SessionManager::sessionStart($sessionName, $lifetime, $cookiePath, $currentDomain,$https);
if (!isset($_SESSION['user_id'])) {
    header("HTTP/1.0 401");
    die();
}
if(isset($_SERVER['HTTP_REFERER'])&&parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST) == DOMAIN){
    if(isset($_GET['role'])){
        $location = "";
        if(password_verify("Madaba",$_GET['role'])){
            $location = "Madaba";
        }elseif (password_verify("Jabal Amman",$_GET['role'])) {
            $location = "Jabal Amman";
        }
        if($location){
            include_once __DIR__ . '/../includes/Inventory.php';

            $data = new Inventory();
            $content = $data->listMedicineInfo();
            $info = array();
            foreach($content as $row){
                $info[$row['medicine_name']] = "({$row['medicine_pills_in_sachet']}) items in sachet and ({$row['medicine_sachet_in_box']}) sachet in box";
            }
            header('Content-Type: application/json');
            echo json_encode($info);
        }
    }
}
?>
