<?php
include_once __DIR__.'/../includes/dir.php';
include_once __DIR__.'/../includes/SessionManager.php';
SessionManager::sessionStart($sessionName, $lifetime, $cookiePath, $currentDomain,$https);
if (!isset($_SESSION['user_id'])) {
    header("HTTP/1.0 401");
    die();
}
if(isset($_SERVER['HTTP_REFERER'])&&parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST) == DOMAIN){
    if(isset($_GET['validate'])){
        $location = "";
        $location = isset($_SESSION['user_role'])?$_SESSION['user_role']:'';

        if($location&&isset($_GET['validate'])&&isset($_GET['data'])){
            $type = $_GET['validate'];
            $value = $_GET['data'];
            switch ($type) {
                case 'perscription_id':
                    include_once __DIR__ . '/../includes/Perscription.php';
                    $persrciption = new Perscription();
                    if(!$persrciption->checkExistIdInLocation($value,$location)){
                        echo 1;
                    }else{
                        echo 0;
                    }
                    break;

                case 'delivery_id_in_notebook':
                    include_once __DIR__ . '/../includes/Delivery.php';
                    $delivery = new Delivery();
                    if(!$delivery->checkDeliveryId($value)){
                        echo 1;
                    }else{
                        echo 0;
                    }
                    break;
                case 'medicine_name':
                    include_once __DIR__ . '/../includes/Inventory.php';
                    $inventory = new Inventory();
                    $result = $inventory->checkMedicineNameExist($value);
                    header('Content-Type: application/json');
                    echo json_encode($result);
                    break;

                    default:
                    header("HTTP/1.0 400");
                    die();
                    break;
            }

        }
    }
}
?>
