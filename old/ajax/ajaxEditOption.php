<?php
include_once __DIR__.'/../includes/dir.php';
include_once __DIR__.'/../includes/SessionManager.php';
SessionManager::sessionStart($sessionName, $lifetime, $cookiePath, $currentDomain,$https);
if (!isset($_SESSION['user_id'])) {
    header("HTTP/1.0 401");
    die();
}

if(isset($_SERVER['HTTP_REFERER'])&&parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST) == DOMAIN&&isset($_GET['role'])){
    $location = "";
    if(password_verify("Madaba",$_GET['role'])){
        $location = "Madaba";
    }elseif (password_verify("Jabal Amman",$_GET['role'])) {
        $location = "Jabal Amman";
    }else{
        header("HTTP/1.0 401");
        die();
    }
    if(isset($_GET['id'])&&isset($_GET['type'])){
        switch ($_GET['type']) {
            case 'inventory':
           // {"medicine-name":"Alka-U","medicine-type":"powder","num-of-sachet":"10","num-of-pills":"1","num-of-boxes-available":"5","num-of-sachets-available":"0","num-of-pills-available":"0"}
                if(isset($_POST['medicine-name'])&&isset($_POST['medicine-type'])&&isset($_POST['num-of-sachet'])&&isset($_POST['num-of-pills'])&&isset($_POST['num-of-boxes-available'])&&isset($_POST['num-of-sachets-available'])&&isset($_POST['num-of-pills-available'])){
                    include_once __DIR__.'/../includes/Inventory.php';
                    $inventory = new Inventory();
                    $result = $inventory->editMedicineInfo($_GET['id'],$location,$_POST['medicine-name'],$_POST['medicine-type'],$_POST['num-of-sachet'],$_POST['num-of-pills'],$_POST['num-of-boxes-available'],$_POST['num-of-sachets-available'],$_POST['num-of-pills-available']);
                    header("HTTP/1.0 {$result['status']} {$result['info']}");
                }else{
                    header("HTTP/1.0 400 missing values");
                }
                break;

            default:
                header("HTTP/1.0 400 not valid type");
                die();
            # code...
            break;
        }
        die();
    }else{

    }
}else{
    header("HTTP/1.0 401");
    die();
}
