<?php
include_once __DIR__.'/../includes/dir.php';
include_once __DIR__.'/../includes/SessionManager.php';
SessionManager::sessionStart($sessionName, $lifetime, $cookiePath, $currentDomain,$https);
if (!isset($_SESSION['user_id'])) {
    header("HTTP/1.0 401");
    die();
}
if(isset($_SERVER['HTTP_REFERER'])&&parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST) == DOMAIN){
    if(isset($_GET['role'])){
        // $token = (time()/60)%(60*24);
        $location = "";
        if(password_verify("Madaba",$_GET['role'])){
            $location = "Madaba";
        }elseif (password_verify("Jabal Amman",$_GET['role'])) {
            $location = "Jabal Amman";
        }

        if($location){
            include_once __DIR__ . '/../includes/Inventory.php';
            $inventory = new Inventory();
            $list = array();
            $sqlResult = $inventory->listMedicineWithAmount($location);
            foreach ($sqlResult as $key => $value) {
                # code...
                $list[] = array("medicine_name"=>$value['medicine_name'],"medicine_amount"=>getAmountString($value['inventory_count_quantity_in_pills'],$value['medicine_pills_in_sachet'],$value['medicine_sachet_in_box']));
            }
            header('Content-Type: application/json');
            echo json_encode($list);
        }
    }
}

function getAmountString($total,$pills_in_sachet,$sachet_in_box){
    $sign = 1;
    if($total < 0){
        $total *= -1;
        $sign = -1;
    }
    $boxes = floor($total/($pills_in_sachet*$sachet_in_box))*$sign;
    $remainder = ($total%($pills_in_sachet*$sachet_in_box));
    $sachets = floor($remainder/$pills_in_sachet)*$sign;
    $pills = ($remainder%$pills_in_sachet)*$sign;
    $first = 0;
    $result = "";
    if($boxes){ $result .= "($boxes)boxes"; $first = 1;}
    if($sachets){ $result .= $first?", ($sachets)sachets":"($sachets)sachets"; $first = 1;}
    $result .= $first?$pills?", ($pills)pills":"":"($pills)pills";
    return $result;

}
?>
