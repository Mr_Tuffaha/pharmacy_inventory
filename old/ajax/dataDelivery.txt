{
  "data": [
    {
      "receipt": "1",
      "id_note_book": "1",
      "deliverer": "1",
      "date": "Tiger Nixon",
      "content": [
        {
          "medicine": "med name",
          "quantity": "123"
        },
        {
          "medicine": "med name",
          "quantity": "2324"
        },
        {
          "medicine": "med name",
          "quantity": "123der"
        },
        {
          "medicine": "med name",
          "quantity": "123der"
        }
      ]
    },
    {
      "receipt": "2",
      "id_note_book": "1",
      "deliverer": "1",
      "date": "Tiger Nixon",
      "content": [
        {
          "medicine": "med name",
          "quantity": "123der"
        },
        {
          "medicine": "med name",
          "quantity": "123der"
        },
        {
          "medicine": "med name",
          "quantity": "123der"
        },
        {
          "medicine": "med name",
          "quantity": "123der"
        }
      ]
    }
  ]
}
