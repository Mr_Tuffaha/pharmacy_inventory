<?php
include_once __DIR__.'/../includes/dir.php';
include_once __DIR__.'/../includes/SessionManager.php';
SessionManager::sessionStart($sessionName, $lifetime, $cookiePath, $currentDomain,$https);
if (!isset($_SESSION['user_id'])) {
    header("HTTP/1.0 401");
    die();
}
if(isset($_SERVER['HTTP_REFERER'])&&parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST) == DOMAIN){
    if(isset($_GET['role'])){
        $location = "";
        if(password_verify("Madaba",$_GET['role'])){
            $location = "Madaba";
        }elseif (password_verify("Jabal Amman",$_GET['role'])) {
            $location = "Jabal Amman";
        }
        if($location){
            include_once __DIR__ . '/../includes/Inventory.php';

            $data = new Inventory();
            $content = $data->fetchAllByLocation($location);

            $result = array();
            $resultInner = array();
            foreach ($content as $row) {
                # code...
                $sign = 1;
                if($row['inventory_count_quantity_in_pills'] < 0){
                    $row['inventory_count_quantity_in_pills'] *= -1;
                    $sign = -1;
                }
                $boxes = floor($row['inventory_count_quantity_in_pills']/($row['medicine_pills_in_sachet']*$row['medicine_sachet_in_box']))*$sign;
                $remainder = ($row['inventory_count_quantity_in_pills']%($row['medicine_pills_in_sachet']*$row['medicine_sachet_in_box']));
                $sachets = floor($remainder/$row['medicine_pills_in_sachet'])*$sign;
                $pills = ($remainder%$row['medicine_pills_in_sachet'])*$sign;
                $resultInner[] = $row['medicine_name'];
                $resultInner[] = $row['medicine_type'];
                $resultInner[] = $boxes;
                $resultInner[] = $sachets;
                $resultInner[] = $pills;
                $resultInner[] = '<p class="row-edit">Edit</p>';
                // $resultInner[] = '<p class="row-edit">Edit</p>/<p class="row-delete">Delete</p>';
                $resultInner[] = $row['medicine_id'];
                // $resultInner[] = $row['inventory_count_quantity_in_pills'];
                $resultInner[] = $row['medicine_pills_in_sachet'];
                $resultInner[] = $row['medicine_sachet_in_box'];
                $result['data'][] = $resultInner;
                $resultInner = array();
            }
            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }
}
?>
