function viewImage(input) {
    // console.log(input);
    if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $('#body-main .profile-image').attr('src', e.target.result);
    }
    reader.readAsDataURL(input.files[0]);
  }
}

$(document).ready(function() {

    // $('#change-password-form').disableAutoFill();
});
