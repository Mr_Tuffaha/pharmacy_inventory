function showMsg(msg,type) {
    var error = '<div class="error-msg msg"><i class="fa fa-times-circle"></i>'+msg+'</div>';
    var warning = '<div class="warning-msg msg"><i class="fa fa-warning"></i>'+msg+'</div>';
    var success = '<div class="success-msg msg"><i class="fa fa-check"></i>'+msg+'</div>';
    var info = '<div class="info-msg msg"><i class="fa fa-info-circle"></i>'+msg+'</div>';
    switch (type) {
        case 'error':
        return error;
        break;
        case 'warning':
        return warning;
        break;
        case 'success':
        return success;
        break;
        case 'info':
        return info;
        break;
        default:

    }

}


$(document).ready(function () {
    // $("#login-form").disableAutoFill();
    $(".form-input-submit").click(function(){
        $(".msg").remove();
        var emptyVar = $(this).parent().parent().find('input').filter(function() { return $(this).val() == ""; });
        if(emptyVar.length){
            $('.form-container > .form-nav-container').after(showMsg("Empty Fields","error"));
            emptyVar.addClass('input-error');
        }else{
            console.log($(this).parent().parent().submit());
            // $('.form-container > .form-nav-container').after(showMsg("Submited","info"));
        }
    });

    $("#login-btn").click(function(){

        $(".msg").remove();
        $('.form').removeClass('hide');
        $('.signup-form').addClass('hide');
    });


    $("#signup-btn").click(function(){
        $(".msg").remove();
        $('.form').removeClass('hide');
        $('.login-form').addClass('hide');
    });
});
