var globalTable = null;

function getNumberFormat(number) {
    if(number<10){
        return '0'+number;
    }else{
        return number;
    }
}

function getHoursPAM(hour){
    if(hour > 12){
        return 'PM';
    }else{
        return 'AM';
    }
}

function getFullDateText() {
    var datetime = new Date();
    var dateText = getNumberFormat(datetime.getDate())+'/'+getNumberFormat(datetime.getMonth()+1)+'/'+getNumberFormat(datetime.getFullYear())+' '+getNumberFormat(datetime.getHours()%12)+':'+getNumberFormat(datetime.getMinutes())+' '+getHoursPAM(datetime.getHours());
    return dateText;
}

function modelHandler(modalEdit,rowData,medName,table){

    modalEdit.find(".edit").css('display', 'grid');
    modalEdit.find(".loading").css('display', 'none');
    modalEdit.find(".done").css('display', 'none');
    modalEdit.css('display', 'grid');
    $("#edit-medicine-cancle-btn, #edit-medicine-close-btn").on('click',this,function(){
        modalEdit.css('display', 'none');
        modalEdit.find('.edit').css('display', 'grid');
        modalEdit.find('.loading').css('display', 'none');
        modalEdit.find('.done').css('display', 'none');

    });
    $('#edit-medicine-modal-content').on('input','input[type=text][name!=medicine-name]',function() {
        $(this).val($(this).val().replace(/(?!-?\d+)\D/g,''));
    });
    $('#edit-medicine-modal-content').on('blur','input[type=text][name!=medicine-name]',function() {
        $(this).val($(this).val().replace(/(?!-?\d+)\D/g,''));
    });
    $('#edit-medicine-modal-content').on('blur','input[type=text][class!=optional]',function() {
        if(!$(this).val()){
            $(this).addClass('input-error');
        }else{
            $(this).removeClass('input-error');
        }
    });

    $('#edit-medicine-modal-content #medicine-name-fields').on('input','input[name=medicine-name]',function() {
        var container = $('#edit-medicine-modal-content #medicine-name-fields');
        container.find("i.fa-spinner").remove();
        var inputField = container.find('input[name=medicine-name]');
        var oldName = rowData[0].toLowerCase();

        var id = $(this).val();
        if(id!=""){
            if(id.toLowerCase()!=oldName){
                container.find(".msg").remove();
                container.append('<i class="fas fa-spinner"></i>');
                $.ajax({
                    url: "ajax/ajaxValidation.php?validate=medicine_name&data="+id,
                    context: container
                }).done(function(data) {
                    $( this ).find("i.fa-spinner").remove();
                    $( this ).find(".msg").remove();
                    inputField.removeClass('input-error');
                    inputField.removeClass('input-success');
                    if(!data[0]||(data[1]!=null&&data[1].toLowerCase()==oldName)){
                        inputField.addClass('input-success');
                    }else{
                        inputField.addClass('input-error');
                        container.append(showMsg('name already used','error'));
                    }
                });
            }else{
                inputField.addClass('input-success');
            }
        }else{
            container.find("i.fa-spinner").remove();
            container.find("div.msg").remove();
            inputField.removeClass('input-success');
        }
    });

    $("#edit-medicine-btn").click(function(){
        // $(".msg").remove();
        $('input[type=text]').removeClass('input-error');
        $('input[type=text]').removeClass('input-success');
        var emptyVar = $('#edit-medicine-modal-content').find('input[type=text][class!=optional]').filter(function() { return $(this).val() == ""; });
        if(emptyVar.length){
            $('#edit-medicine-modal-content > .modal-header > h2').after(showMsg("Empty Fields","error"));
            emptyVar.addClass('input-error');
        }else{
            if(modalEdit.find(".error-msg").filter(function() { return $(this).text() == "name already used"; }).length==0){
                $(".msg").remove();
                $('input[type=text]').removeClass('input-error');
                var oldData = [rowData[0],rowData[1],rowData[8],rowData[7],rowData[2],rowData[3],rowData[4]]
                var newDate = modalEdit.find('input[type=text],select');
                var changeValue = 0;
                var length = oldData.length;
                for(i=0;i<length;i++){
                    console.log(oldData[i]+' : '+newDate.slice(i).val());
                    if(oldData[i] != newDate.slice(i).val()){
                        changeValue = 1;
                        break;
                    }
                }
                if(changeValue){
                    console.log('hello');
                    modalEdit.find(".edit").css('display', 'none');
                    modalEdit.find(".loading").css('display', 'grid');
                    $("#edit-medicine-btn").off();
                    $.ajax({
                        type:"POST",
                        data:newDate,
                        url: "ajax/ajaxEditOption.php?role="+enctRole+"&type=inventory&id="+rowData[6],
                    }).done(function(data) {
                        table.ajax.reload(function() {
                        modalEdit.find(".notice").css('display', 'none');
                        modalEdit.find(".loading").css('display', 'none');
                        modalEdit.find(".done").css('display', 'grid');
                        });
                    }).fail(function(data,status){
                        console.log(data.status+': '+data.statusText);
                        table.ajax.reload();
                        modalEdit.find(".notice").css('display', 'none');
                        modalEdit.find(".loading").css('display', 'none');
                        modalEdit.find(".done").css('display', 'grid');
                    });
                }else{
                    $('#edit-medicine-modal-content > .modal-header > .msg').remove();
                    $('#edit-medicine-modal-content > .modal-header > h2').after(showMsg("No Change found","error"));
                }

            }else{
                $('#edit-medicine-modal-content > .modal-header > .msg').remove();
                $('#edit-medicine-modal-content > .modal-header > h2').after(showMsg("fix the name","error"));

            }
            // $('#add-medicine-form').submit();
        }

    });
}


//$(document).tooltip();
$(document).ready(function () {
    // console.log(getFullDateText());
    var datetime = new Date();
    var table = $('#myTable').DataTable({
        "ajax" : "ajax/ajaxInventory.php?role="+enctRole,
        columns: [
            { title: "Name" },
            { title: "Type" },
            { title: "Boxes" },
            { title: "Sachets" },
            { title: "Pills" },
            { title: "Options","orderable": false }
        ],
        stateSave: true,
        responsive: true,
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        dom: 'Blfrtip',
        buttons: [{
            extend: 'excel',
            title: 'GJU Pharmacy Inventory',
            filename: 'Pharmacy_inventory_' + datetime.getDate() + '_' + (datetime.getMonth()+1) + '_' + datetime.getFullYear(),
        },
        {
            extend: 'pdf',
            text: 'PDF',
            exportOptions: {
                columns: ':visible'
            },
            title: 'GJU Pharmacy Inventory',
            pageSize: 'A4',
            // download: 'open',
            filename: 'Pharmacy_inventory_' + datetime.getDate() + '_' + (datetime.getMonth()+1) + '_' + datetime.getFullYear(),
            customize: function ( doc ) {
                doc['footer']=(function(page, pages) {
                    return {
                        columns: [
                            getFullDateText(),
                            {
                                alignment: 'right',
                                text: [
                                    { text: page.toString(), italics: true },
                                    ' of ',
                                    { text: pages.toString(), italics: true }
                                ]
                            }
                        ],
                        margin: [10, 0]
                    }
                });
            }
        },'colvis'],
        "initComplete": function(settings, json) {
            globalTable = table;
            $('#myTable tbody').tooltip({
                items: 'tr',
                // track:true,
                position:{
                    my: "center top-19",
                    at: "center top-19"
                },
                show: {
                    delay: 1500,
                    duration: 0
                },
                content: function(){
                    return '('+table.row(this).data()[7]+') items in sachet and ('+table.row(this).data()[8]+') sachet in box';
                }
            });
        }
    });



    $('#myTable tbody').on('click','p',function(){
        var type = $(this).attr('class');
        var id = table.row($(this).parent().parent()).data()[6]
        if(type=='row-edit'){
            // alert('edit: '+id);
        }else if (type=='row-delete') {
            // alert('delete: '+id);
        }
    });

    // Get the modal
    var editModal = $('#edit-medicine-modal');

    // Get the <span> element that closes the modal
    var editSpan = $("#edit-medicine-modal .close");

    $('#myTable tbody').on('click','p',function(){
        var type = $(this).attr('class');
        var row = table.row($(this).parent().parent());
        var id = row.data()[6];
        var medName = row.data()[0];
        if(type=='row-edit'){
            var modalEdit = $('#edit-medicine-modal');
            var medType = row.data()[1];
            var pill_in_sachet = row.data()[7];
            var sachet_in_box = row.data()[8];
            var availableBoxes = row.data()[2];
            var availableSachets = row.data()[3];
            var availablePills = row.data()[4];
            modalEdit.find(".msg").remove();
            modalEdit.find('input[type=text]').removeClass('input-error');
            modalEdit.find('input[type=text]').removeClass('input-success');
            modalEdit.find('input[type=text]').val('');
            modalEdit.find('input[name=med-id]').val(id);
            modalEdit.find('input[name=old-name]').val(medName);
            modalEdit.find('input[name=medicine-name]').val(medName);
            modalEdit.find('select[name=medicine-type]').val(medType);
            modalEdit.find('input[name=num-of-sachet]').val(sachet_in_box);
            modalEdit.find('input[name=num-of-pills]').val(pill_in_sachet);
            modalEdit.find('input[name=num-of-boxes-available]').val(availableBoxes);
            modalEdit.find('input[name=num-of-sachets-available]').val(availableSachets);
            modalEdit.find('input[name=num-of-pills-available]').val(availablePills);
            modelHandler(modalEdit,row.data(),medName,table);
            // $('#edit-medicine-modal-content').off();
        }else if (type=='row-delete') {
            var deleteModal = $("#delete-notice");
            deleteModal.find('.notice').css('display', 'grid');
            deleteModal.find('.loading').css('display', 'none');
            deleteModal.find('.done').css('display', 'none');
            deleteModal.css('display', 'grid');
            deleteModal.find('p.target').text('Medicine: '+medName);
            $("#delete-notice-delete-btn").on('click',this,function(){
                $.ajax({
                    url: "ajax/ajaxDeleteOption.php?role="+enctRole+"&type=inventory&id="+id,
                }).done(function(data) {
                    table.ajax.reload(function() {
                    deleteModal.find(".notice").css('display', 'none');
                    deleteModal.find(".loading").css('display', 'none');
                    deleteModal.find(".done").css('display', 'grid');
                    });
                }).fail(function(data,status){
                    console.log(data.status+': '+data.statusText);
                    table.ajax.reload();
                    deleteModal.find(".notice").css('display', 'none');
                    deleteModal.find(".loading").css('display', 'none');
                    deleteModal.find(".done").css('display', 'grid');
                });
                deleteModal.find(".notice").css('display', 'none');
                deleteModal.find(".loading").css('display', 'grid');
                deleteModal.find(".done").css('display', 'none');
            });
            $("#delete-notice-close-btn").on('click',this,function(){
                deleteModal.css('display', 'none');
                deleteModal.find(".notice").css('display', 'grid');
                deleteModal.find(".loading").css('display', 'none');
                deleteModal.find(".done").css('display', 'none');
            });
            $("#delete-notice-cancle-btn").on('click',this,function(){
                deleteModal.css('display', 'none');
                deleteModal.find(".notice").css('display', 'grid');
                deleteModal.find(".loading").css('display', 'none');
                deleteModal.find(".done").css('display', 'none');
            });
        }
    });


    // When the user clicks on <span> (x), close the modal
    editSpan.on('click',this,function(){
        editModal.css('display', 'none');
    });
    //
    // // When the user clicks anywhere outside of the modal, close it
    // editModal.on('click',this,function(e){
    //     // console.log(e);
    //     if (e.target == this) editModal.css('display', 'none');
    // });





});
