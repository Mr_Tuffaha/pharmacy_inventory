/* Formatting function for row details - modify as you need */
function formatContentAmount(sachet_in_box,pills_in_sachet,amount) {
    var boxes = Math.floor(amount/(sachet_in_box*pills_in_sachet));
    var remainder = Math.floor(amount%(sachet_in_box*pills_in_sachet));
    var sachet = Math.floor(remainder/pills_in_sachet);
    var pills = Math.floor(remainder%pills_in_sachet);
    var result = "";
    if(boxes>0){
        result += "("+boxes+")Boxes ";
    }
    if(sachet>0){
        result += "("+sachet+")Sachets ";
    }
    if(pills>0){
        result += "("+pills+")Pills ";
    }
    return result;
}

function format ( data ) {
    var returnTable = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
    '<thead>'+
    '<tr>'+
    '<th>Medicine</th>'+
    '<th>Quantity</th>'+
    '</tr>'+
    '</thead>'+
    '<tbody>';

    //data.forEach(function(element) {
    //  console.log(element);
    //});
    for (var i = 0, len = data.content.length; i < len; i++) {
        returnTable +=
        '<tr>'+
        '<td>'+data.content[i].medicine_name+'</td>'+
        '<td>'+formatContentAmount(data.content[i].medicine_sachet_in_box,data.content[i].medicine_pills_in_sachet,data.content[i].quantity)+'</td>'+
        '</tr>'
    }
    returnTable +='</tbody></table>';

    return returnTable;
}



function listOptions(medicineNames){
    // console.log(medicineNames);
    var options = "";
    for(var i=0,len = medicineNames.length;i<len;i++){
        options += '<option value="'+medicineNames[i]['medicine_name']+'">'+medicineNames[i]['medicine_name']+' '+medicineNames[i]['medicine_amount']+'</option>';
    }

    return options;
}

function newElement(medicineNames){
    var newSet = '<div class="form-element list form-element-optional"><label for="">Medicine:</label><select class="medicine-select" name="medicine[]">';
    newSet += listOptions(medicineNames);
    newSet += '</select><input type="text" name="quantity[]" value="" placeholder="number of pills Ex:0"><i class="fa fa-times-circle btn close-btn"  aria-hidden="true"></i></div>';
    return newSet;
}

function getOptionsList(medicineNames) {
    $(".medicine-select").append(listOptions(medicineNames));
}

function modelHandler(modalEdit,rowData,medName){
    modalEdit.find('input[type=text]').val('');
    modalEdit.find('.form-element-optional').remove();
    $("#edit-perscription-cancle-btn, #edit-perscription-close-btn").on('click',this,function(){
        modalEdit.css('display', 'none');
        $("#add-element").off();
        modalEdit.find('.edit').css('display', 'grid');
        modalEdit.find('.loading').css('display', 'none');
        modalEdit.find('.done').css('display', 'none');

    });
    modalEdit.find('input[name=perscription_id]').val(rowData['number']);
    modalEdit.find('input[name=student_employe_id]').val(rowData['patiant_id']);
    modalEdit.find('input[name=patiant_type]').val(rowData['patiant_type']);
    getOptionsList(medName);
    $("#add-element").on('click',this,function(){
        $("fieldset").append(newElement(medName));
    });
    var contentCount = rowData['content'].length;
    for(i=2;i<=contentCount;i++){
        $("fieldset").append(newElement(medName));
    }
    var tempSelect = $("fieldset").find('select[name="medicine[]"]');
    var tempInput = $("fieldset").find('input[name="quantity[]"]');
    for(i=0;i<contentCount;i++){
        tempSelect.slice(i).val(rowData['content'][i]['medicine_name']);
        tempInput.slice(i).val(rowData['content'][i]['quantity']);
    }
    modalEdit.css('display', 'grid');
    $('#perscription_id_fields').on('input','input[name=perscription_id]',function() {
        $('#perscription_id_fields').find("i.fa-spinner").remove();
        var inputField = $('#perscription_id_fields').find('input[name=perscription_id]');
        $(this).val($(this).val().replace(/\D/g,''));
        var id = $(this).val();
        if(id!=""&&id!=rowData['number']){
            $('#perscription_id_fields').append('<i class="fas fa-spinner"></i>');
            $.ajax({
                url: "ajax/ajaxValidation.php?validate=perscription_id&data="+id,
                context: $('#perscription_id_fields')
            }).done(function(data) {
                $( this ).find("i.fa-spinner").remove();
                $( this ).find("div.msg").remove();
                inputField.removeClass('input-error');
                inputField.removeClass('input-success');
                if(data==1){
                    inputField.addClass('input-success');
                }else{
                    inputField.addClass('input-error');
                    $('#perscription_id_fields').append(showMsg('id already used','error'));
                }
            });
        }else if(id!=""&&id==rowData['number']){
            inputField.addClass('input-success');
        }else{
            $('#perscription_id_fields').find("i.fa-spinner").remove();
            $('#perscription_id_fields').find("div.msg").remove();
            inputField.removeClass('input-success');
        }
    });
    $('form').on('input','input[type=text][name!=deliverer][name!=medicine-name][name!=id-in-receipt]',function() {
        $(this).val($(this).val().replace(/\D/g,''));
    });
    $('form').on('blur','input[type=text][name!=deliverer][name!=medicine-name][name!=id-in-receipt]',function() {
        $(this).val($(this).val().replace(/\D/g,''));
    });
    $('form').on('blur','input[type=text][class!=optional]',function() {
        if(!$(this).val()){
            $(this).addClass('input-error');
        }else{
            $(this).removeClass('input-error');
        }
    });

    $("#add-element").prop('disabled',false);
    $("#submit-btn").prop('disabled',false);
    $("fieldset").on("click",".close-btn",function(){
        $(this).parent().remove();
    });
    $("fieldset").on("click",".close-btn",function(){
        $(this).parent().remove();
    });

    $("#edit-perscription-btn").on('click',this,function(){
        if(modalEdit.find(".error-msg").filter(function() { return $(this).text() == "id already used"; }).length==0){
            modalEdit.find(".msg").remove();
            modalEdit.find('input[type=text]').removeClass('input-error');
            var emptyVar = $('#edit-perscription-form input[type=text][class!=optional]').filter(function() { return $(this).val() == ""; });
            if(emptyVar.length){
                modalEdit.find('#edit-perscription-form').prepend(showMsg("Empty Fields","error"));
                emptyVar.addClass('input-error');
            }else{
                var headData = modalEdit.find('#edit-perscription-form > .form-element').find('input[type=text],select');
                var contentData =  modalEdit.find('#edit-perscription-form fieldset .form-element').find('input[type=text],select');
                modalEdit.find('.edit').css('display','none');
                modalEdit.find('.done').css('display','grid');
            }
        }else{
            modalEdit.find("#edit-perscription-form > .msg").remove();
            modalEdit.find('#edit-perscription-form').prepend(showMsg("fix the id","error"));

        }
    });
}

// var medicineNames[];
$(document).ready(function () {
    var datetime = new Date();
    var table = $('#myTable').DataTable({
        "ajax": "ajax/ajaxPerscription.php?role="+enctRole,
        "columns": [
            {
                title:'',
                "className":      'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            },
            { title:'Number',"data": "number" },
            { title:'Patiant Id',"data": "patiant_type_id"},
            { title:'Date',"data": "date" },
            { title:'Note',"data": "note","width": "20%" },
            {title:'Options',"orderable": false, "data": "options" }
        ],
        responsive: true,
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        dom: 'Blfrtip',
        buttons: [
            {
                extend: 'colvis',
                columns: ':gt(0)'
            }],
            "order": [[ 1, "desc" ]]
        });


        // Add event listener for opening and closing details
        $('#myTable tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row( tr );

            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                // console.log(row.data());
                row.child( format(row.data()) ).show();
                tr.addClass('shown');
            }
        } );


        $('#myTable tbody').on('click','p',function(){
            var type = $(this).attr('class');
            var row = table.row($(this).parent().parent());
            if(type=='row-edit'){
                // var modalEdit = $('#edit-perscription-modal');
                // $("#add-element").prop('disabled',true);
                // $("#submit-btn").prop('disabled',true);
                // $.get( {url:"ajax/ajaxList.php?role="+enctRole,data: function( data ) {
                // },success:function(data) {
                //     modelHandler(modalEdit,row.data(),data);
                // }});// modalEdit.find('input[name=num-of-pills-available]').val(availablePills);

            }else if (type=='row-delete') {
                var id = row.data()['number'];
                var deleteModal = $("#delete-notice");
                deleteModal.find('.notice').css('display', 'grid');
                deleteModal.find('.loading').css('display', 'none');
                deleteModal.find('.done').css('display', 'none');
                deleteModal.css('display', 'grid');
                deleteModal.find('p.target').text('Perscription: '+id);
                $("#delete-notice-delete-btn").on('click',this,function(){
                    console.log('delete');
                    deleteModal.find(".notice").css('display', 'none');
                    deleteModal.find(".loading").css('display', 'grid');
                    $.ajax({
                        url: "ajax/ajaxDeleteOption.php?role="+enctRole+"&type=perscription&id="+id,
                    }).done(function(data) {
                        table.ajax.reload(function() {
                            deleteModal.find(".notice").css('display', 'none');
                            deleteModal.find(".loading").css('display', 'none');
                            deleteModal.find(".done").css('display', 'grid');
                        });
                    }).fail(function(data,status){
                        console.log(data.status+': '+data.statusText);
                        table.ajax.reload();
                        deleteModal.find(".notice").css('display', 'none');
                        deleteModal.find(".loading").css('display', 'none');
                        deleteModal.find(".done").css('display', 'grid');
                    });
                    // deleteModal.find(".done").css('display', 'none');
                });
                $("#delete-notice-close-btn").on('click',this,function(){
                    deleteModal.css('display', 'none');
                    deleteModal.find(".notice").css('display', 'grid');
                    deleteModal.find(".loading").css('display', 'none');
                    deleteModal.find(".done").css('display', 'none');
                    $("#delete-notice-delete-btn").off();
                });
                $("#delete-notice-cancle-btn").on('click',this,function(){
                    deleteModal.css('display', 'none');
                    deleteModal.find(".notice").css('display', 'grid');
                    deleteModal.find(".loading").css('display', 'none');
                    deleteModal.find(".done").css('display', 'none');
                    $("#delete-notice-delete-btn").off();
                });
            }
        });

    });
