/* Formatting function for row details - modify as you need */
function format ( data ) {
    var returnTable = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
    '<thead>'+
    '<tr>'+
    '<th>Medicine</th>'+
    '<th>Quantity</th>'+
    '</tr>'+
    '</thead>'+
    '<tbody>';

    //data.forEach(function(element) {
    //  console.log(element);
    //});
    for (var i = 0, len = data.content.length; i < len; i++) {
        returnTable +=
        '<tr>'+
        '<td>'+data.content[i].medicine_name+'</td>'+
        '<td>'+data.content[i].quantity_in_boxes+'</td>'+
        '</tr>'
    }
    returnTable +='</tbody></table>';

    return returnTable;
}



$(document).ready(function () {
    var datetime = new Date();
    var table = $('#myTable').DataTable({
        "ajax": "ajax/ajaxDelivery.php?role="+enctRole,
        "columns": [
            {
                title:'',
                "className":      'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            },
            { title:'Id in reciept',"data": "receipt" },
            { title:'Id in notebook',"data": "id_note_book" },
            { title:'Deliverer',"data": "deliverer" },
            { title:'Date',"data": "date" },
            {title:'Options',"orderable": false, "data": "options" }
        ],
        responsive: true,
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        dom: 'Blfrtip',
        buttons: [
            // {
            //     extend: 'excel',
            //     title: 'GJU Pharmacy Inventory',
            //     filename: 'Pharmacy_inventory_' + datetime.getDate() + '_' + (datetime.getMonth()+1) + '_' + datetime.getFullYear(),
            // },
            // {
            //     extend: 'pdf',
            //     text: 'PDF',
            //     exportOptions: {
            //         columns: ':visible'
            //     },
            //     title: 'GJU Pharmacy Inventory',
            //     pageSize: 'A4',
            //     download: 'open',
            //     filename: 'Pharmacy_inventory_' + datetime.getDate() + '_' + (datetime.getMonth()+1) + '_' + datetime.getFullYear(),
            //     customize: function ( doc ) {
            //         doc['footer']=(function(page, pages) {
            //             return {
            //                 columns: [
            //                     '',
            //                     {
            //                         alignment: 'right',
            //                         text: [
            //                             { text: page.toString(), italics: true },
            //                             ' of ',
            //                             { text: pages.toString(), italics: true }
            //                         ]
            //                     }
            //                 ],
            //                 margin: [10, 0]
            //             }
            //         });
            //     }
            // },
            {
                extend: 'colvis',
                columns: ':gt(0)'
            }],
            "order": [[ 2, "desc" ]]
        });


        // Add event listener for opening and closing details
        $('#myTable tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row( tr );

            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                console.log(row.data());
                row.child( format(row.data()) ).show();
                tr.addClass('shown');
            }
        });

        // Add event listener for opening and closing details
        $('#myTable tbody').on('click', 'p', function () {
            var type = $(this).attr('class');
            var row = table.row($(this).parent().parent());
            if (type=='row-delete') {
                var id = row.data()['id_note_book'];
                var deleteModal = $("#delete-notice");
                deleteModal.find('.notice').css('display', 'grid');
                deleteModal.find('.loading').css('display', 'none');
                deleteModal.find('.done').css('display', 'none');
                deleteModal.css('display', 'grid');
                deleteModal.find('p.target').text('Delivery: '+id);
                $("#delete-notice-delete-btn").on('click',this,function(){
                    deleteModal.find(".notice").css('display', 'none');
                    deleteModal.find(".loading").css('display', 'grid');
                    $.ajax({
                        url: "ajax/ajaxDeleteOption.php?role="+enctRole+"&type=delivery&id="+id,
                    }).done(function(data) {
                        table.ajax.reload(function() {
                            deleteModal.find(".notice").css('display', 'none');
                            deleteModal.find(".loading").css('display', 'none');
                            deleteModal.find(".done").css('display', 'grid');
                        });
                    }).fail(function(data,status){
                        console.log(data.status+': '+data.statusText);
                        table.ajax.reload(function() {
                            deleteModal.find(".notice").css('display', 'none');
                            deleteModal.find(".loading").css('display', 'none');
                            deleteModal.find(".done").css('display', 'grid');
                        });
                    });
                    deleteModal.find(".done").css('display', 'none');
                });
                $("#delete-notice-close-btn").on('click',this,function(){
                    deleteModal.css('display', 'none');
                    deleteModal.find(".notice").css('display', 'grid');
                    deleteModal.find(".loading").css('display', 'none');
                    deleteModal.find(".done").css('display', 'none');
                    $("#delete-notice-delete-btn").off();
                });
                $("#delete-notice-cancle-btn").on('click',this,function(){
                    deleteModal.css('display', 'none');
                    deleteModal.find(".notice").css('display', 'grid');
                    deleteModal.find(".loading").css('display', 'none');
                    deleteModal.find(".done").css('display', 'none');
                    $("#delete-notice-delete-btn").off();
                });
            }
        });
    });
