var medicineNames = [];



function listOptions(medicineNames){
    // console.log(medicineNames);
    var options = "";
    for(var i=0,len = medicineNames.length;i<len;i++){
        options += '<option value="'+medicineNames[i]['medicine_name']+'">'+medicineNames[i]['medicine_name']+' '+medicineNames[i]['medicine_amount']+'</option>';
    }

    return options;
}

function newElement(typeElement,medicineNames){
    var newSet = '<div class="form-element list"><label for="">Medicine:</label><select class="" name="medicine[]">';
    newSet += listOptions(medicineNames);
    if(typeElement){
        newSet += '</select><input type="text" name="quantity[]" value="" placeholder="number of pills Ex:0"><i class="fa fa-times-circle btn close-btn"  aria-hidden="true"></i></div>';
    }else{
        newSet += '</select><input type="text" name="quantity[]" placeholder="number of boxes" value=""><i class="fa fa-times-circle btn close-btn"  aria-hidden="true"></i></div>';
    }
    return newSet;
}

function getOptionsList(medicineNames) {
    $(".medicine-select").append(listOptions(medicineNames));
}

$(document).ready(function () {

    $("#add-element").prop('disabled',true);
    $("#submit-btn").prop('disabled',true);
    $.get( {url:"ajax/ajaxList.php?role="+enctRole,data: function( data ) {
        // console.log(JSON.parse(data));
    },success:function(data) {
        medicineNames = data;
        getOptionsList(medicineNames);
        $("#add-element").prop('disabled',false);
        $("#submit-btn").prop('disabled',false);
    }});



    $("#add-element").click(function(){
        if($('#add-perscription-form').length){
            $("fieldset").append(newElement(1,medicineNames));
        }else{
            $("fieldset").append(newElement(0,medicineNames));
        }
    });

    $("fieldset").on("click",".close-btn",function(){
        $(this).parent().remove();
    });

    $('form').on('input','input[type=text][name!=deliverer][name!=medicine-name][name!=id-in-receipt]',function() {
        $(this).val($(this).val().replace(/\D/g,''));
    });
    $('form').on('blur','input[type=text][name!=deliverer][name!=medicine-name][name!=id-in-receipt]',function() {
        $(this).val($(this).val().replace(/\D/g,''));
    });
    $('form').on('blur','input[type=text][class!=optional]',function() {
        if(!$(this).val()){
            $(this).addClass('input-error');
        }else{
            $(this).removeClass('input-error');
        }
    });

    $('#perscription_id_fields').on('input','input[name=perscription_id]',function() {
        $('#perscription_id_fields').find("i.fa-spinner").remove();
        var inputField = $('#perscription_id_fields').find('input[name=perscription_id]');
        $(this).val($(this).val().replace(/\D/g,''));

        var id = $(this).val();
        if(id!=""){
            $('#perscription_id_fields').append('<i class="fas fa-spinner"></i>');
            $.ajax({
                url: "ajax/ajaxValidation.php?validate=perscription_id&data="+id,
                context: $('#perscription_id_fields')
            }).done(function(data) {
                $( this ).find("i.fa-spinner").remove();
                $( this ).find("div.msg").remove();
                inputField.removeClass('input-error');
                inputField.removeClass('input-success');
                if(data==1){
                    inputField.addClass('input-success');
                }else{
                    inputField.addClass('input-error');
                    $('#perscription_id_fields').append(showMsg('id already used','error'));
                }
            });
        }else{
            $('#perscription_id_fields').find("i.fa-spinner").remove();
            $('#perscription_id_fields').find("div.msg").remove();
            inputField.removeClass('input-success');
        }
    });

    $('#id-in-notebook-fields').on('input','input[name=id-in-notebook]',function() {

        $('#id-in-notebook-fields').find("i.fa-spinner").remove();
        var inputField = $('#id-in-notebook-fields').find('input[name=id-in-notebook]');
        $(this).val($(this).val().replace(/\D/g,''));
        var id = $(this).val();
        if(id!=""){
            $('#id-in-notebook-fields').append('<i class="fas fa-spinner"></i>');
            $.ajax({
                url: "ajax/ajaxValidation.php?validate=delivery_id_in_notebook&data="+id,
                context: $('#id-in-notebook-fields')
            }).done(function(data) {
                $( this ).find("i.fa-spinner").remove();
                $( this ).find("div.msg").remove();
                inputField.removeClass('input-error');
                inputField.removeClass('input-success');
                if(data==1){
                    inputField.addClass('input-success');
                }else{
                    inputField.addClass('input-error');
                    $('#id-in-notebook-fields').append(showMsg('id already used','error'));
                }
            });
        }else{
            $('#id-in-notebook-fields').find("i.fa-spinner").remove();
            $('#id-in-notebook-fields').find("div.msg").remove();
            inputField.removeClass('input-success');
        }
    });




    $("#submit-btn").click(function(){
        if($(".error-msg").filter(function() { return $(this).text() == "id already used"; }).length==0){

            $(".msg").remove();
            $('input[type=text]').removeClass('input-error');
            var emptyVar = $('#add-delivery-form input[type=text][class!=optional], #add-perscription-form input[type=text][class!=optional]').filter(function() { return $(this).val() == ""; });
            if(emptyVar.length){
                $('#body-main > .divider').after(showMsg("Empty Fields","error"));
                emptyVar.addClass('input-error');
            }else{
                $('form').submit();
            }
        }else{
            $('#body-main > .msg').remove();
            $('#body-main > .divider').after(showMsg("fix the id","error"));

        }
    });
});
