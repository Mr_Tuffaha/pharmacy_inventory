
$(document).ready(function () {


    // Get the modal
    var addModal = $('#add-medicine-modal');

    // Get the button that opens the modal
    var addBtn = $("#add-medicine-btn");

    // Get the <span> element that closes the modal
    var addSpan = $("#add-medicine-modal .close");

    // When the user clicks the button, open the modal

    addBtn.on('click',this,function(){
        console.log('htub');
        addModal.css('display', 'grid');
    });


    // When the user clicks on <span> (x), close the modal
    addSpan.on('click',this,function(){
        addModal.css('display', 'none');
    });

    // When the user clicks anywhere outside of the modal, close it
    addModal.on('click',this,function(e){
        // console.log(e);
        if (e.target == this) addModal.css('display', 'none');
    });


    addModal.on('input','#add-medicine-modal-content input[type=text][name!=medicine-name]',function() {
        $(this).val($(this).val().replace(/\D/g,''));
    });

    addModal.on('blur','input[type=text][name!=medicine-name]',function() {
        $(this).val($(this).val().replace(/\D/g,''));
    });

    addModal.on('blur','input[type=text][class!=optional]',function() {
        if(!$(this).val()){
            $(this).addClass('input-error');
        }else{
            $(this).removeClass('input-error');
        }
    });

    $('#add-medicine-name-fields').on('input','input[name=medicine-name]',function() {
        var container = $('#add-medicine-name-fields')
        container.find("i.fa-spinner").remove();
        var inputField = container.find('input[name=medicine-name]');

        var id = $(this).val();
        if(id!=""){
            container.append('<i class="fas fa-spinner"></i>');
            $.ajax({
                url: "ajax/ajaxValidation.php?validate=medicine_name&data="+id,
                context: container,
                beforeSend: function(){
                    inputField.removeClass('input-error');
                    inputField.removeClass('input-success');
                }
            }).done(function(data) {
                // delay(3);
                $( this ).find("i.fa-spinner").remove();
                $( this ).find("div.msg").remove();
                inputField.removeClass('input-error');
                inputField.removeClass('input-success');
                if(!data[0]){
                    inputField.addClass('input-success');
                }else{
                    inputField.addClass('input-error');
                    container.append(showMsg('name already used','error'));
                }
            });
        }else{
            container.find("i.fa-spinner").remove();
            container.find("div.msg").remove();
            inputField.removeClass('input-success');
        }
    });

    $("#add-medicine-submit-btn").click(function(){
        $(".msg").remove();
        $('input[type=text]').removeClass('input-error');
        var emptyVar = $('#add-medicine-form').find('input[type=text][class!=optional]').filter(function() { return $(this).val() == ""; });
        if(emptyVar.length){
            $('#add-medicine-modal-content > .modal-header > h2').after(showMsg("Empty Fields","error"));
            emptyVar.addClass('input-error');
        }else{
            $('#add-medicine-form').submit();
        }
    });
});
