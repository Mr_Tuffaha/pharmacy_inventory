<?php
include_once __DIR__.'/includes/SessionManager.php';
SessionManager::sessionStart($sessionName, $lifetime, $cookiePath, $currentDomain ,$https);

$timeoutLength = 180;
if(!isset($_SESSION['failed_login_count'])){
    if(isset($_SESSION['failed_login_timer'])) unset($_SESSION['failed_login_timer']);
    $_SESSION['failed_login_count'] = 0;
}
if($_SESSION['failed_login_count']>2&&!isset($_SESSION['failed_login_timer'])){
    $_SESSION['failed_login_timer'] = time();
}

$error_message = "";
if(!$mode){
    header("location: fixing.php");
    die();
}

if (isset($_SESSION['user_id'])) {
    header("location: index.php");
}

if(isset($_SESSION['failed_login_timer'])&&(time()-$_SESSION['failed_login_timer'])<$timeoutLength){
    $timeLeft = $timeoutLength-(time()-$_SESSION['failed_login_timer']);
   $error_message = "maximum incorrect logins, please wait: $timeLeft s";
}else if(isset($_SESSION['failed_login_timer'])){
    unset($_SESSION['failed_login_timer']);
    $_SESSION['failed_login_count'] = 0;
}

if(!isset($_SESSION['failed_login_timer'])){
    if(isset($_POST['type'])){
        if($_POST['type']=='login'){
            include_once __DIR__.'/includes/User.php';
            $user = new User();
            $result = $user->auth($_POST['username'], $_POST['password'],0);
            if($result){
                $_SESSION['user_id'] = $result['user_id'];
                $_SESSION['user_firstname'] = $result['user_firstname'];
                $_SESSION['user_lastname'] = $result['user_lastname'];
                $_SESSION['user_email'] = $result['user_email'];
                $_SESSION['user_role'] = $result['user_role'];
                $_SESSION['user_image'] = $result['user_image'];
                unset($_SESSION['failed_login_timer']);
                unset($_SESSION['failed_login_count']);
                header("location: index.php");
            }else{
                $error_message = " Incorrect username or password!";
                $_SESSION['failed_login_count']++;
            }
        }elseif ($_POST['type']=='signup') {
            # code...
        }
    }
}
 ?>

<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <meta name="author" content="Omar Tuffaha">
    <meta name="description" content="">
    <meta property="og:image" content="">
    <meta property="og:description" content="">
    <meta property="og:title" content="">
    <title>Pharmacy Login</title>
    <link rel='stylesheet' href='css/fontawesome-free-5.0.9/web-fonts-with-css/css/fontawesome-all.min.css'>
    <link rel="stylesheet" href="css/login.css">

</head>
<body>
    <div class="body-container">
        <header class="body-header">
            <div class="header-logo">
                <h1>GJU</h1>
                <h3>Pharmacy</h3>
            </div>
            <div class="header-other">

            </div>
        </header>
        <main class="body-main" id="body-main">

            <div class="form-container">
                <div class="form-nav-container">
                    <button type="button" class="form-nav btn" id="login-btn" name="add">Login</button>
                    <button type="button" class="form-nav btn" id="signup-btn" name="submitbtn">Signup</button>
                </div>

                <?php if($error_message){ ?>
                <div class="error-msg msg"><i class="fa fa-times-circle"><?php echo $error_message;?></i></div>
                <?php } ?>
                
                <form id="login-form" class="form login-form" action="" method="post" autocomplete="off">
                    <div class="form-title">
                        <h1>Login</h1>
                    </div>
                    <input type="hidden" name="type" value="login">
                    <div class="form-fieldset">
                        <label class="form-label" for="">Username:</label>
                        <input class="form-input" type="text" name="username" value=""  placeholder="username">
                    </div>
                    <div class="form-fieldset">
                        <label class="form-label" for="">Password:</label>
                        <input class="form-input" type="password" name="password" value=""  placeholder="password">
                    </div>
                    <div class="form-submit">
                        <input class="form-input-submit btn" type="button" name="login" value="Submit">
                    </div>
                </form>


                <form class="form signup-form hide"  action="" method="post" autocomplete="off">
                    <div class="form-title">
                        <h1>Sign Up</h1>
                    </div>
                    <input type="hidden" name="type" value="signup">
                    <div class="form-fieldset">
                        <label class="form-label" for="">Email:</label>
                        <input class="form-input" placeholder="Email" type="text" name="email" value="">
                    </div>
                    <div class="form-fieldset">
                        <label class="form-label" for="">Username:</label>
                        <input class="form-input" placeholder="username" type="text" name="username" value="">
                    </div>
                    <div class="form-fieldset">
                        <label class="form-label" for="">Password:</label>
                        <input class="form-input" placeholder="password" type="password" name="password" value="">
                    </div>
                    <div class="form-fieldset">
                        <label class="form-label" for="">RePassword:</label>
                        <input class="form-input" placeholder="password" type="password" name="repassword" value="">
                    </div>
                    <div class="form-submit">
                        <input class="form-input-submit btn" type="button" name="signup" value="Submit">
                    </div>
                </form>

            </div>


        </main>
        <footer class="body-footer">
            <p id="copyright" property="dc:rights">&copy;
              <span property="dc:dateCopyrighted"><?php echo date("Y");?></span>
              <span property="dc:publisher">German Jordanian University</span>
            </p>
        </footer>


    </div>
    <script src='js/jquery-3.3.1.min.js'></script>
    <script type="text/javascript" src="js/login.js"></script>
</body>
</html>
