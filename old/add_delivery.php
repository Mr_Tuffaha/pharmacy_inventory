<?php
include_once 'php_head.php';
$pageName = "add_delivery";
$title = "Pharmacy Delivery";
$dicription = "";

if(isset($_POST['enct'])){
    if($_POST['enct']&&$_POST['medicine-name']&&$_POST['medicine-type']&&$_POST['num-of-sachet']&&$_POST['num-of-pills']){
        include_once __DIR__.'/includes/Inventory.php';
        $inventory = new Inventory();
        $inventory->insertNewMedicine($_POST['medicine-name'],$_POST['medicine-type'],$_POST['num-of-sachet'],$_POST['num-of-pills']);
        //header('location: index.php');
    }
}

if(isset($_POST['id-in-notebook'])){
    include_once __DIR__.'/includes/Delivery.php';
    $delivery = new Delivery();
    $content = array();
    $length = sizeof($_POST['medicine']);
    for ($i=0; $i < $length; $i++) {
        $content[] = array($_POST['medicine'][$i],$_POST['quantity'][$i]);
    }
    // var_dump($content);
    $delivery->insertDelivery($_POST['id-in-receipt'],$_POST['id-in-notebook'],$_POST['deliverer'],$_SESSION['user_id'],$content);
    header('location: delivery.php');
}


include_once __DIR__.'/header.php';
include_once __DIR__.'/aside.php';
include_once __DIR__.'/modal/modal.php';
?>

<main class="body-main" id="body-main">
    <h1>Add Delivery</h1>
    <div class="divider"></div>
    <form class="add-form" id="add-delivery-form" action="" method="post">
        <div class="form-element" >
            <label for="">ID in receipt:</label>
            <input type="text" name="id-in-receipt" value="" placeholder="ID in receipt">
        </div>
        <div class="form-element" id="id-in-notebook-fields">
            <label for="">ID in notebook:</label>
            <input type="text" name="id-in-notebook" value="" placeholder="ID in notebook">
        </div>
        <div class="form-element">
            <label for="">Deliverer:</label>
            <input type="text" name="deliverer" value="" placeholder="Deliverer">
        </div>
        <fieldset>
            <legend>List</legend>
            <div class="form-element list">
                <label for="">Medicine:</label>
                <select class="medicine-select" name="medicine[]">
                </select>
                <input type="text" name="quantity[]" value="" placeholder="number of boxes">
            </div>
        </fieldset>
    </form>
    <div class="form-element">
        <button type="button" class="green-btn btn" id="add-element" name="add">Add more</button>
        <button type="button" class="submit-btn btn" id="submit-btn" name="submitbtn">Submit</button>
    </div>
    <button id="add-medicine-btn" class="add-medicine-btn btn">Add New Medicine</button>

</main>

<?php
include_once 'footer.php';
include_once 'scripts_and_end_page.php';
?>
