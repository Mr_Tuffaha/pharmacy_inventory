<?php
include_once __DIR__.'/includes/SessionManager.php';
SessionManager::sessionStart($sessionName, $lifetime, $cookiePath, $currentDomain ,$https);
// var_dump($_SESSION);
if (isset($_SESSION['user_id'])) {
    session_unset();
    session_destroy();
    if(isset($_COOKIE['autologin'])){
        include_once __DIR__.'/includes/Autologin.php';
        Autologin::deleteToken();
    }
}
if($mode){
    header("location: login.php");
    die();
}
 ?>

<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <meta name="author" content="Omar Tuffaha">
    <meta name="description" content="">
    <meta property="og:image" content="">
    <meta property="og:description" content="">
    <meta property="og:title" content="">
    <title>Pharmacy</title>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="css/fixing.css">

</head>
<body>
    <div class="body-container">
        <header class="body-header">
            <div class="header-logo">
                <h1>GJU</h1>
                <h3>Pharmacy</h3>
            </div>
            <div class="header-other">

            </div>
        </header>
        <main class="body-main" id="body-main">
            <h1>Sorry the site is being maintained</h1>
            <h2>It will be back up in 1 hour</h2>
        </main>
        <footer class="body-footer">
            <p id="copyright" property="dc:rights">&copy;
              <span property="dc:dateCopyrighted"><?php echo date("Y");?></span>
              <span property="dc:publisher">German Jordanian University</span>
            </p>
        </footer>


    </div>
    <script
    src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>
    <script type="text/javascript" src="js/login.js"></script>
</body>
</html>
