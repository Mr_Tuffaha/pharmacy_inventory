<?php
include_once __DIR__.'/Database.php';
include_once __DIR__.'/Inventory.php';


/**
*
*/
class Perscription extends Database{


    public function deleteSinglePerscription($id,$location)
    {
        # code...
        $id = $this->run_mysql_real_escape_string($id);
        $location = $this->run_mysql_real_escape_string($location);
        $perscriptionData = null;
        if($this->isInteger($id)){
            $query = "SELECT `prescription_id`, `prescription_id_on_paper`, `prescription_patiant_id`, `prescription_patiant_type`, `prescription_date`, `perscription_user`, `perscription_location` FROM `prescription` WHERE `prescription_id_on_paper` = '$id' AND `perscription_location` = '$location';";
            if($this->performQuery($query)){
                $perscriptionData = parent::fetchAll()[0];
            }else{
                return array("status"=>500,"info"=>"something went wrong");
            }
        }else{
            return array("status"=>400,"info"=>"id is not an integer");
        }
        if($perscriptionData){
            $perscriptionContent = null;
            $query = "SELECT `prescription_content_id`, `prescription_content_prescription_id`, `medicine_id`, `quantity` FROM `prescription_content` JOIN `inventory` ON `inventory`.`medicine_id` = `prescription_content`.`prescription_content_inventory_id` WHERE `prescription_content_prescription_id` = '{$perscriptionData['prescription_id']}'";
            if($this->performQuery($query)){
                $perscriptionContent = parent::fetchAll();
            }else{
                return array("status"=>400,"info"=>"something went wrong in the query");
            }
        }else{
            return array("status"=>400,"info"=>"id is not found in database");
        }
        $query = "DELETE FROM `prescription` WHERE `prescription_id_on_paper` = '$id' AND `perscription_location` = '$location';";
        if(!$this->performQuery($query)){
            return array("status"=>500,"info"=>"something went wrong");
        }
        $inventory = new Inventory();
        foreach ($perscriptionContent as $key => $value) {
            $inventory->editAmount($value['quantity'],$value['medicine_id'],$location);
        }
        return array("status"=>200,"info"=>"");
    }

    public function checkExistIdInLocation($id,$location)
    {
        $id = $this->run_mysql_real_escape_string($id);
        $location = $this->run_mysql_real_escape_string($location);
        if($this->isInteger($id)){
            $query = "SELECT `prescription_id_on_paper` FROM `prescription` WHERE `prescription_id_on_paper` = '$id' AND `perscription_location` = '$location';";
            if($this->performQuery($query)){
                return parent::fetchAll();
            }else{
                return FALSE;
            }
        }
    }

    public function insertPerscription($id,$userId,$patiantId,$patiantType,$location,$content,$note)
    {
        $id = $this->run_mysql_real_escape_string($id);
        $userId = $this->run_mysql_real_escape_string($userId);
        $patiantId = $this->run_mysql_real_escape_string($patiantId);
        $patiantType = $this->run_mysql_real_escape_string($patiantType);
        $location = $this->run_mysql_real_escape_string($location);
        $note = $this->run_mysql_real_escape_string($note);
        $timedate = date('Y-m-d H:i:s',time());

        $query = "INSERT INTO `prescription`(`prescription_id_on_paper`,`prescription_patiant_id`, `prescription_patiant_type`, `prescription_date`, `perscription_user`, `perscription_location`,`perscription_note`) VALUES ('$id','$patiantId','$patiantType','$timedate','$userId','$location','$note');";
        // echo $query;
        $inventory = new Inventory();
        if ($this->performQuery($query)) {
            if($content){
                $lastId = $this->lastInsertedId();
                $query = "INSERT INTO `prescription_content`(`prescription_content_prescription_id`, `prescription_content_inventory_id`, `quantity`) VALUES ";
                $first = 1;
                foreach ($content as $key => $value) {
                    // $value[0] = $this->run_mysql_real_escape_string($value[0]);
                    
                    $value[0] = $inventory->getMedicineId($value[0]);
                    $value[1] = $this->run_mysql_real_escape_string($value[1]);
                    if ($first) {
                        $query .= "('$lastId','$value[0]','$value[1]')";
                        $first = 0;
                    }else{
                        $query .= ",('$lastId','$value[0]','$value[1]')";
                    }
                }
                echo $query;

                if ($this->performQuery($query)) {
                    foreach ($content as $key => $value) {
                        $medicineId = $inventory->getMedicineId($value[0]);
                        // $medicineId = $inventory->getMedicineId($value[0]);
                        $inventory->editAmount($value[1]*(-1),$medicineId,$location);
                    }
                    return TRUE;
                }else {
                    return FALSE;
                }
            }else {
                return FALSE;
            }
        }else{
            return FALSE;
        }
    }

    public function fetchAllByLocation($location){
        $location = $this->run_mysql_real_escape_string($location);
        if($location=="Madaba"||$location=="Jabal Amman"){
            $query = "SELECT `prescription_id`, `prescription_id_on_paper`,`prescription_patiant_id`, `prescription_patiant_type`, `prescription_date`, `perscription_user`, `perscription_location`,`perscription_note` FROM `prescription` WHERE `perscription_location` = '$location';";
            if ($this->performQuery($query)) {
                $headContent = parent::fetchAll();
                $fullContent = array();
                $fullContent['data'] = array();
                if($headContent){
                    foreach ($headContent as $row) {
                        $tempRow = array();
                        $tempRow['patiant_type'] = $row['prescription_patiant_type'];
                        $tempRow['patiant_id'] = $row['prescription_patiant_id'];
                        $tempRow['patiant_type_id'] =  $row['prescription_patiant_type'].': '.($row['prescription_patiant_id']?$row['prescription_patiant_id']:'not assigned');
                        $tempRow['number'] = $row['prescription_id_on_paper'];
                        $tempRow['date'] = $row['prescription_date'];
                        $tempRow['note'] = $row['perscription_note'];
                        $tempRow['options'] = '<p class="row-delete">Delete</p>';
                        $id = $row['prescription_id_on_paper'];
                        $query = "SELECT `medicine_sachet_in_box`, `medicine_pills_in_sachet`, `inventory`.`medicine_name`, `quantity` FROM `inventory` JOIN `prescription_content` JOIN `prescription` ON`inventory`.`medicine_id` = `prescription_content`.`prescription_content_inventory_id` AND `prescription`.`prescription_id` = `prescription_content`.`prescription_content_prescription_id`WHERE `prescription`.`prescription_id_on_paper` = '$id';";
                        if ($this->performQuery($query)) {
                            $tempRow['content'] = parent::fetchAll();
                        }else {
                            return FALSE;
                        }
                        $fullContent['data'][] = $tempRow;
                    }
                }
                return $fullContent;
            } else {
                return FALSE;
            }
        }
    }
}//end of class


?>
