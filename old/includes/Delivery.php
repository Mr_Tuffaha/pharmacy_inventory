<?php
include_once __DIR__.'/Database.php';
include_once __DIR__.'/Inventory.php';


/**
*
*/
class Delivery extends Database{


    public function deleteSingleDelivery($delivery_id_in_notebook)
    {
        $delivery_id_in_notebook = $this->run_mysql_real_escape_string($delivery_id_in_notebook);
        $deliveryData = null;
        if($this->isInteger($delivery_id_in_notebook)){
            $query = "SELECT `delivery_id`, `delivery_id_in_receipt`, `delivery_id_in_notebook`, `delivery_date`, `deliverer`, `delivery_user` FROM `delivery` WHERE `delivery_id_in_notebook` = '$delivery_id_in_notebook';";
            if($this->performQuery($query)){
                $deliveryData = parent::fetchAll()[0];
            }else{
                return array("status"=>500,"info"=>"something went wrong");
            }
        }else{
            return array("status"=>400,"info"=>"id is not an integer");
        }
        if($deliveryData){
            $deliveryContent = null;
            $query = "SELECT `delivery_content_id`, `delivery_id_on_paper`, `inventory`.`medicine_name`, `quantity_in_boxes`, `inventory`.`medicine_id`,`inventory`.`medicine_sachet_in_box`,`inventory`.`medicine_pills_in_sachet`FROM `delivery_content` JOIN `inventory` ON `delivery_content`.`delivery_content_inventory_id` = `inventory`.`medicine_id` WHERE `delivery_id_on_paper` = '$delivery_id_in_notebook';";
            if($this->performQuery($query)){
                $deliveryContent = parent::fetchAll();
            }else{
                return array("status"=>400,"info"=>"something went wrong in the query");
            }
        }else{
            return array("status"=>400,"info"=>"id is not found in database");
        }
        $query = "DELETE FROM `delivery` WHERE `delivery_id_in_notebook` = '$delivery_id_in_notebook';";
        if(!$this->performQuery($query)){
            return array("status"=>500,"info"=>"something went wrong");
        }
        $inventory = new Inventory();
        foreach ($deliveryContent as $key => $value) {
            $inventory->editAmount($value['quantity_in_boxes']*$value['medicine_sachet_in_box']*$value['medicine_pills_in_sachet']*(-1),$value['medicine_id'],'Madaba');
        }
        return array("status"=>200,"info"=>"");
    }



    public function insertDelivery($delivery_id_in_receipt,$delivery_id_in_notebook,$deliverer,$delivery_user_id,$content)
    {
        $delivery_id_in_receipt = $this->run_mysql_real_escape_string($delivery_id_in_receipt);
        $delivery_id_in_notebook = $this->run_mysql_real_escape_string($delivery_id_in_notebook);
        $deliverer = $this->run_mysql_real_escape_string($deliverer);
        $delivery_user_id = $this->run_mysql_real_escape_string($delivery_user_id);
        $datetime = date('Y-m-d H:i:s',(time()-(time()%(60*60*24))));
        if($content&&$this->isInteger($delivery_id_in_notebook)&&!$this->checkDeliveryId($delivery_id_in_notebook)){
            $query = "INSERT INTO `delivery`(`delivery_id_in_receipt`, `delivery_id_in_notebook`, `delivery_date`, `deliverer`, `delivery_user`) VALUES ('$delivery_id_in_receipt','$delivery_id_in_notebook','$datetime','$deliverer','$delivery_user_id');";
      
            $inventory = new Inventory();
            if ($this->performQuery($query)) {
                $query = "INSERT INTO `delivery_content`( `delivery_id_on_paper`, `delivery_content_inventory_id`, `quantity_in_boxes`) VALUES ";
                $first = 1;
                foreach ($content as $key => $value) {
                    $value[0] = $inventory->getMedicineId($value[0]);
                    $value[1] = $this->run_mysql_real_escape_string($value[1]);
                    if ($first) {
                        $query .=  "('$delivery_id_in_notebook','$value[0]','$value[1]')";
                        $first = 0;
                    }else{
                        $query .= ",('$delivery_id_in_notebook','$value[0]','$value[1]')";
                    }
                }
                $query .= ";";
                if ($this->performQuery($query)) {
                    foreach ($content as $key => $value) {
                        $medicineId = $inventory->getMedicineId($value[0]);
                        $boxPills = $inventory->getPillsInBox($medicineId);
                        $inventory->editAmount($value[1]*$boxPills,$medicineId,'Madaba');
                    }
                    return TRUE;
                }else {
                    return FALSE;
                }
            } else {
                return FALSE;
            }
        } else {
            return "wrong input";
        }
    }


    public function checkDeliveryId($id)
    {
        $id = $this->run_mysql_real_escape_string($id);
        if($this->isInteger($id)){
            $query = "SELECT `delivery_id_in_notebook` FROM `delivery` WHERE `delivery_id_in_notebook` = '$id';";
            if ($this->performQuery($query)->num_rows==1) {
                return TRUE;
            } else {
                return FALSE;
            }
        }

    }

    public function fetchAll(){
        $query = "SELECT `delivery_id`, `delivery_id_in_receipt`, `delivery_id_in_notebook`, `delivery_date`, `deliverer` FROM `delivery`";
        if ($this->performQuery($query)) {
            $headContent = parent::fetchAll();
            $fullContent = array();
            $fullContent['data'] = array();
            if($headContent){
                foreach ($headContent as $row) {
                    $tempRow = array();
                    $tempRow['receipt'] = $row['delivery_id_in_receipt'];
                    $tempRow['id_note_book'] = $row['delivery_id_in_notebook'];
                    $tempRow['deliverer'] = $row['deliverer'];
                    $tempRow['date'] = $row['delivery_date'];
                    // $tempRow['options'] = '<p class="row-edit">Edit</p>/<p class="row-delete">Delete</p>';
                    $tempRow['options'] = '<p class="row-delete">Delete</p>';
                    $id = $row['delivery_id_in_notebook'];
                    $query = "SELECT `inventory`.`medicine_name`, `quantity_in_boxes` FROM `delivery_content` JOIN `inventory` ON `medicine_id` = `delivery_content_inventory_id` WHERE `delivery_id_on_paper` = '$id';";
                    if ($this->performQuery($query)) {
                        $tempRow['content'] = parent::fetchAll();
                    }else {
                        return FALSE;
                    }
                    $fullContent['data'][] = $tempRow;
                }
            }
            return $fullContent;
        } else {
            return NULL;
        }
    }
}

?>
