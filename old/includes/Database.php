<?php

/**
 * @ClassName : MySql Database Class
 * @Description : This Class is Used to perform actions in Mysql DB
 * @Version : 2.7.2v
 * @LastEdit : 5/Dec/2017
 * @Author : Omar Tuffaha <omar_tuffaha@hotmail.com>
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 */

require_once __DIR__ . '/config.php';

class Database {

    private $conn;
    private $lastQuery;
    private $recordSet = array();

    /**
     * this is a contructer of the class datbase and it runs openConnection()
     */
    public function __construct() {

        //it start the connection with the database
        $this->openConnection();
    }

//end of construct

    /**
     * this function establishes a connection to database
     */
    private function openConnection() {

        //connect to database
        $this->conn = mysqli_connect(DBSERVER, DBUSER, DBPASS, DBNAME);

        //if connection did not establish the website will die (stop)
        if (!$this->conn) {
            die("Cannot Connect to Server: " . mysqli_connect_error());
        }//end of if statment

        if (!mysqli_set_charset($this->conn, "utf8")) {
            die("Error loading character set utf8: " . mysqli_connect_error() . "\n");
            exit();
        }
    }

//end of openConnection()

    /**
     * this function performs a query that was passed to this function
     * @param String query
     * @return mixed query data or False
     */
    protected function performQuery($query) {

        //perform query
        $this->lastQuery = mysqli_query($this->conn, $query);

        // if the query is performed it will return the result else it
        // will return false
        return $this->lastQuery ? $this->lastQuery : FALSE;
    }

//end of function performQuery($query)

    /**
     * this function performs multi insert queries that was passed to this function
     * @param String queries
     * @return mixed query data or False
     */
    protected function insertMultiQuery($querys) {

        //perform query
        $this->lastQuery = mysqli_multi_query($this->conn, $querys);

        // if the query is performed it will return the result else it
        // will return false
        return $this->lastQuery ? $this->lastQuery : FALSE;
    }

//end of performMultiQuery()

    /**
     * this function returns all of a list that was requested from a database
     * @return Array recordSet of all data
     */
    protected function fetchAll() {

        $this->recordSet = NULL;
        //this loop goes over all the items that are requested
        while ($row = mysqli_fetch_assoc($this->lastQuery)) {
            $this->recordSet[] = $row;
        }//end of while loop
        //if the recordset is not empty the record set the result else it
        // will return false
        return !empty($this->recordSet) ? $this->recordSet : FALSE;
    }//end of function fetchAll


    /**
     * this function returns the last id used
     * @return mixed last inserted id
     */
    protected function lastInsertedId() {
        return mysqli_insert_id($this->conn);
    }//end of lastInserted Id

    /**
     * this function takes a string and runs it in mysqli_escape_string() to put in mysql query
     * @param String string
     * @return String string after mysqli_escape_string()
     */
    protected function run_mysql_real_escape_string($string,$html = 1) {
        if($html){
            $string = htmlspecialchars($string);
        }
        return mysqli_escape_string($this->conn, $string);
    }

    /**
     * gets the last mysqli error
     * @return String mysqli_error()
     */
    protected function getMysqliError() {
        return mysqli_error($this->conn);
    }

    protected function isInteger($input){
        $input = strval($input);
        if ($input[0] == '-') {
            return ctype_digit(substr($input, 1));
        }
        return ctype_digit($input);
    }

}//end of class database
