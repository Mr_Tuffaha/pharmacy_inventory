<?php

include_once __DIR__.'/Database.php';

class InnerDelivery extends Database
{
    public function insertRequest($userId,$content,$note="")
    {
        $timedate = date('Y-m-d H:i:s',time());
        $userId = $this->run_mysql_real_escape_string($userId);
        $note = $this->run_mysql_real_escape_string($note);
        $query = "INSERT INTO `inner_delivery`(`inner_delivery_request_user_id`, `inner_delivery_request_date`, `inner_delivery_request_note`,`inner_delivery_stage`) VALUES ('$userId','$timedate','$note','1');";
        if ($this->performQuery($query)) {
            $lastId = $this->lastInsertedId();
            $query = "INSERT INTO `inner_delivery_content`(`inner_delivery_content_inner_delivery_id`, `inner_delivery_content_stage_num`, `inner_delivery_content_medicine`, `inner_delivery_content_quantity`) VALUES ";
            $first = 1;
            foreach ($content as $key => $value) {
                $value[0] = $this->run_mysql_real_escape_string($value[0]);
                $value[1] = $this->run_mysql_real_escape_string($value[1]);
                if ($first) {
                    $query .= "('$lastId','1','$value[0]','$value[1]')";
                    $first = 0;
                }else{
                    $query .= ",('$lastId','1','$value[0]','$value[1]')";
                }
            }
            echo $query;
            if ($this->performQuery($query)) {
                return TRUE;
            }else{
                return FALSE;
            }
        }else{
            return FALSE;
        }

    }


    public function sendDelivery($deliveryId,$userId,$content,$note="")
    {
        $timedate = date('Y-m-d H:i:s',time());
        $deliveryId = $this->run_mysql_real_escape_string($deliveryId);
        $userId = $this->run_mysql_real_escape_string($userId);
        $note = $this->run_mysql_real_escape_string($note);
        $query = "UPDATE `inner_delivery` SET `inner_delivery_send_user_id`='$userId',`inner_delivery_send_date`='$timedate',`inner_delivery_send_note`='$note',`inner_delivery_stage`= '2' WHERE `inner_delivery_stage` = '1';";
        if ($this->performQuery($query)) {
            $query = "INSERT INTO `inner_delivery_content`(`inner_delivery_content_inner_delivery_id`, `inner_delivery_content_stage_num`, `inner_delivery_content_medicine`, `inner_delivery_content_quantity`) VALUES ";
            $first = 1;
            foreach ($content as $key => $value) {
                $value[0] = $this->run_mysql_real_escape_string($value[0]);
                $value[1] = $this->run_mysql_real_escape_string($value[1]);
                if ($first) {
                    $query .= "('$deliveryId','2','$value[0]','$value[1]')";
                    $first = 0;
                }else{
                    $query .= ",('$deliveryId','2','$value[0]','$value[1]')";
                }
            }
            // echo $query;
            if ($this->performQuery($query)) {
                return TRUE;
            }else{
                return FALSE;
            }
        }else{
            return FALSE;
        }

    }


    public function recieveDelivery($deliveryId,$userId,$content,$note="")
    {
        $timedate = date('Y-m-d H:i:s',time());
        $deliveryId = $this->run_mysql_real_escape_string($deliveryId);
        $userId = $this->run_mysql_real_escape_string($userId);
        $note = $this->run_mysql_real_escape_string($note);
        $query = "UPDATE `inner_delivery` SET `inner_delivery_recieve_user_id`='$userId',`inner_delivery_recieve_date`='$timedate',`inner_delivery_recieve_note`='$note',`inner_delivery_stage`= '3' WHERE `inner_delivery_stage` = '2';";
        if ($this->performQuery($query)) {
            $query = "INSERT INTO `inner_delivery_content`(`inner_delivery_content_inner_delivery_id`, `inner_delivery_content_stage_num`, `inner_delivery_content_medicine`, `inner_delivery_content_quantity`) VALUES ";
            $first = 1;
            foreach ($content as $key => $value) {
                $value[0] = $this->run_mysql_real_escape_string($value[0]);
                $value[1] = $this->run_mysql_real_escape_string($value[1]);
                if ($first) {
                    $query .= "('$deliveryId','3','$value[0]','$value[1]')";
                    $first = 0;
                }else{
                    $query .= ",('$deliveryId','3','$value[0]','$value[1]')";
                }
            }
            if ($this->performQuery($query)) {
                include_once __DIR__ . '/Inventory.php';
                $inventory = new Inventory();
                foreach ($content as $key => $value) {
                    $medicineId = $inventory->getMedicineId($value[0]);
                    $boxPills = $inventory->getPillsInBox($medicineId);
                    $inventory->editAmount($value[1]*$boxPills*(-1),$medicineId,'Madaba');
                    $inventory->editAmount($value[1]*$boxPills,$medicineId,'Jabal Amman');
                }
                return TRUE;

            }else{
                return FALSE;
            }
        }else{
            return FALSE;
        }

    }

    public function fetchCurrentSend(){
       $query = "SELECT `inner_delivery_id`, `inner_delivery_send_user_id`, `inner_delivery_send_date`, `inner_delivery_send_note`,`inner_delivery_request_note` FROM `inner_delivery` WHERE `inner_delivery_stage` = '2' LIMIT 1;";
       if($this->performQuery($query)){
           $result = parent::fetchAll()[0];
           $query = "SELECT  `inner_delivery_content`.`inner_delivery_content_medicine` AS `medicine_name`, `inner_delivery_content`.`inner_delivery_content_quantity` AS `stage_1_count`,`temp_name`,`temp`.`inner_delivery_content_quantity` AS `stage_2_count` FROM `inner_delivery_content` RIGHT JOIN (SELECT  `inner_delivery_content_medicine` AS `temp_name`, `inner_delivery_content_quantity` FROM `inner_delivery_content` WHERE `inner_delivery_content_inner_delivery_id` = '{$result['inner_delivery_id']}' AND `inner_delivery_content_stage_num` = '2') AS `temp` ON `inner_delivery_content`.`inner_delivery_content_medicine` = `temp`.`temp_name` AND `inner_delivery_content_inner_delivery_id` = '{$result['inner_delivery_id']}' AND `inner_delivery_content_stage_num` = '1';";

           //'{$result['inner_delivery_id']}';";
           if($this->performQuery($query)){
               $result['content'] = parent::fetchAll();
               return $result;
           }else{
               return FALSE;
           }
       }else{
           return FALSE;
       }

    }

    public function fetchCurrentRequest(){
       $query = "SELECT `inner_delivery_id`, `inner_delivery_request_user_id`, `inner_delivery_request_date`, `inner_delivery_request_note` FROM `inner_delivery` WHERE `inner_delivery_stage` = '1' LIMIT 1;";
       if($this->performQuery($query)){
           $result = parent::fetchAll()[0];
           $query = "SELECT `inner_delivery_content_medicine` AS `medicine_name`,`num_of_boxes`, `inner_delivery_content_quantity` AS `count` FROM `inner_delivery_content` JOIN (SELECT `medicine_name`, FLOOR(`inventory_count_quantity_in_pills`/(`medicine_sachet_in_box`* `medicine_pills_in_sachet`)) AS `num_of_boxes` FROM `inventory` JOIN `inventory_count`  ON `medicine_id` = `inventory_count_inventory_id` WHERE `inventory_count`.`inventory_count_location` = 'Madaba') AS `temp` ON `temp`.`medicine_name` = `inner_delivery_content`.`inner_delivery_content_medicine` WHERE `inner_delivery_content_inner_delivery_id` = '{$result['inner_delivery_id']}' AND `inner_delivery_content_stage_num` = '1';";
           if($this->performQuery($query)){
               $result['content'] = parent::fetchAll();
               return $result;
           }else{
               return FALSE;
           }
       }else{
           return FALSE;
       }
    }


    public function checkCurrentStage()
    {
        $query = "SELECT `inner_delivery_stage` FROM `inner_delivery` WHERE `inner_delivery_stage` < 3;";
        if($this->performQuery($query)){
            $result =  parent::fetchAll();
            return $result?$result[0]['inner_delivery_stage']:0;
        }else{
            return FALSE;
        }
    }

}


?>
