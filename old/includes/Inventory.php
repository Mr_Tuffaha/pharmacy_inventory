<?php
include_once __DIR__ . '/Database.php';

/**
*
*/
class Inventory extends Database{


    public function editMedicineInfo($id,$location,$name,$type,$sachet_in_box,$pills_in_sachet,$boxs,$sachts,$pills)
    {
        $id = $this->run_mysql_real_escape_string($id);
        $location = $this->run_mysql_real_escape_string($location);
        $name = $this->run_mysql_real_escape_string($name);
        $type = $this->run_mysql_real_escape_string($type);
        $sachet_in_box = $this->run_mysql_real_escape_string($sachet_in_box);
        $pills_in_sachet = $this->run_mysql_real_escape_string($pills_in_sachet);
        $boxs = $this->run_mysql_real_escape_string($boxs);
        $sachts = $this->run_mysql_real_escape_string($sachts);
        $pills = $this->run_mysql_real_escape_string($pills);
        if($this->isInteger($id)&&$this->isInteger($sachet_in_box)&&$this->isInteger($pills_in_sachet)&&$this->isInteger($boxs)&&$this->isInteger($sachts)&&$this->isInteger($pills)){
            $query = "UPDATE `inventory` SET `medicine_name`='$name',`medicine_type`='$type',`medicine_sachet_in_box`='$sachet_in_box',`medicine_pills_in_sachet`='$pills_in_sachet' WHERE `medicine_id` = '$id';";
            if($this->performQuery($query)){
                $query = "";
                $quantity = ($sachet_in_box*$pills_in_sachet*$boxs)+($sachts*$pills_in_sachet)+($pills);
                if($location == 'Madaba'){
                    $query = "UPDATE `inventory_count` SET `inventory_count_quantity_in_pills`='$quantity' WHERE `inventory_count_inventory_id` = '$id' AND `inventory_count_location` = '$location';";

                }else if($location == 'Jabal Amman'){
                    $query = "UPDATE `inventory_count` SET `inventory_count_quantity_in_pills`='$quantity' WHERE `inventory_count_inventory_id` = '$id' AND `inventory_count_location` = '$location';";
                }
                if($query){
                    if($this->performQuery($query)){
                        return array("status"=>200,"info"=>'');
                    }else{
                        return array("status"=>400,"info"=>$this->getMysqliError());
                    }
                }
            }else{
                return array("status"=>400,"info"=>$this->getMysqliError());
            }
        }else{
            return array("status"=>400,"info"=>'incorrect values');
        }

    }



    public function listMedicine(){
        $query = "SELECT `medicine_name` FROM `inventory`;";
        if ($this->performQuery($query)) {
            return parent::fetchAll();
        } else {
            return NULL;
        }
    }

    public function listMedicineWithAmount($location){
        $location = $this->run_mysql_real_escape_string($location);
        $query = "SELECT `medicine_name`, `medicine_sachet_in_box`, `medicine_pills_in_sachet`,`inventory_count_quantity_in_pills` FROM `inventory` JOIN `inventory_count` ON `inventory`.`medicine_id` = `inventory_count`.`inventory_count_inventory_id` AND `inventory_count`.`inventory_count_location` = '$location' ORDER BY `medicine_name`;";
        if ($this->performQuery($query)) {
            return parent::fetchAll();
        } else {
            return NULL;
        }
    }


    public function listMedicineInfo(){
        $query = "SELECT `medicine_name`, `medicine_sachet_in_box`, `medicine_pills_in_sachet` FROM `inventory`;";
        if ($this->performQuery($query)) {
            return parent::fetchAll();
        } else {
            return NULL;
        }
    }

    public function fetchAllByLocation($location)
    {
        $location = $this->run_mysql_real_escape_string($location);
        if($location=="Madaba"||$location=="Jabal Amman"){
            $query = "SELECT `medicine_id`, `medicine_name`, `medicine_type`,`medicine_sachet_in_box`, `medicine_pills_in_sachet`, `inventory_count_quantity_in_pills` FROM `inventory` JOIN `inventory_count` ON `medicine_id` = `inventory_count_inventory_id` WHERE `inventory_count_location` = '$location';";
            if ($this->performQuery($query)) {
                return parent::fetchAll();
            } else {
                return NULL;
            }
        }else{
            return "wrong location";
        }
    }

    public function getPillsInBox($id)
    {
        $id = $this->run_mysql_real_escape_string($id);
        $query = "SELECT `medicine_sachet_in_box`, `medicine_pills_in_sachet` FROM `inventory` WHERE `medicine_id` = '$id';";
        if ($this->performQuery($query)) {
            $row = parent::fetchAll()[0];
            return $row['medicine_sachet_in_box']*$row['medicine_pills_in_sachet'];
        } else {
            return FALSE;
        }
    }

    public function editAmount($amount,$medicineId,$location){
        $amount = $this->run_mysql_real_escape_string($amount);
        $medicineId = $this->run_mysql_real_escape_string($medicineId);
        $location = $this->run_mysql_real_escape_string($location);
        if($this->isInteger($amount)&&($location=="Madaba"||$location=="Jabal Amman")&&$this->isInteger($medicineId)){
            if($this->checkMedicineIdExist($medicineId)){
                $query = "UPDATE `inventory_count` SET `inventory_count_quantity_in_pills`=(`inventory_count_quantity_in_pills`+$amount) WHERE `inventory_count_inventory_id` = '$medicineId' AND `inventory_count_location` = '$location';";
                if ($this->performQuery($query)) {
                    return TRUE;
                } else {
                    return $this->getMysqliError();
                }
            }else{
                return $this->getMysqliError();
            }
        }else{
            return "wrong input";
        }
    }

    public function insertNewMedicine($medicineName,$type,$sachet_in_box,$pills_in_sachet,$current_amount=0){
        $medicineName = $this->run_mysql_real_escape_string($medicineName);
        $type = $this->run_mysql_real_escape_string($type);
        $sachet_in_box = $this->run_mysql_real_escape_string($sachet_in_box);
        $pills_in_sachet = $this->run_mysql_real_escape_string($pills_in_sachet);
        $current_amount = $this->run_mysql_real_escape_string($current_amount);
        if($this->isInteger($sachet_in_box)&&$this->isInteger($pills_in_sachet)&&$this->isInteger($current_amount)){
            if(!$this->checkMedicineNameExist($medicineName)[0]){
                $query = "INSERT INTO `inventory`(`medicine_name`, `medicine_type`, `medicine_sachet_in_box`, `medicine_pills_in_sachet`) VALUES ('$medicineName','$type','$sachet_in_box','$pills_in_sachet');";
                if ($this->performQuery($query)) {
                    $id = $this->lastInsertedId();
                    $query =  "INSERT INTO `inventory_count`(`inventory_count_inventory_id`, `inventory_count_location`, `inventory_count_quantity_in_pills`) VALUES ('$id','Madaba','$current_amount'),('$id','Jabal Amman','0');\n";
                    //echo $query;
                    if ($this->performQuery($query)) {
                        return TRUE;
                    }else{
                        return $this->getMysqliError();
                    }
                } else {
                    return $this->getMysqliError();
                }
            }else{
                return FALSE;
            }
        }
    }

    public function deleteMedinine($id)
    {
        $id = $this->run_mysql_real_escape_string($id);
        $query = "DELETE FROM `inventory` WHERE `medicine_id` = '$id';";
        if ($this->performQuery($query)) {
            return array(200,'');
        }else{
            return array(400,"{$this->getMysqliError()}");
        }
    }

    private function checkMedicineIdExist($id){
        $id = $this->run_mysql_real_escape_string($id);
        $query = "SELECT `medicine_id` FROM `inventory` WHERE `medicine_id` = '$id';";
        if ($this->performQuery($query)->num_rows==1) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function checkMedicineNameExist($name){
        $name = $this->run_mysql_real_escape_string($name);
        $query = "SELECT `medicine_name` FROM `inventory` WHERE `medicine_name` = '$name';";
        if ($this->performQuery($query)->num_rows==1) {
            return array(TRUE,parent::fetchAll()[0]['medicine_name']);
        } else {
            return array(FALSE,NULL);
        }
    }

    public function getMedicineId($name){
        $name = $this->run_mysql_real_escape_string($name);
        $query = "SELECT `medicine_id` FROM `inventory` WHERE `medicine_name` = '$name';";
        if ($this->performQuery($query)) {
            return parent::fetchAll()[0]['medicine_id'];
        } else {
            return FALSE;
        }
    }
}






?>
