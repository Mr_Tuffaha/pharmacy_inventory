<?php
include_once __DIR__.'/php_head.php';
$pageName = "profile";
$title = "Pharmacy Profile";
$dicription = "";

if(isset($_POST['submit-password-btn'])){
    include_once __DIR__.'/includes/User.php';
    $user = new User();
    if($user->authPassword($_SESSION['user_id'],$_POST['old-password'])){
        if($_POST['new-password']==$_POST['re-password']){
            $user->updatePassword($_SESSION['user_id'],$_POST['new-password']);
        }else{
            echo "string";
        }
    }
}

if(isset($_POST['submit-info-btn'])){
    include_once __DIR__.'/includes/User.php';
    $user = new User();
    $user->setFirstname($_POST['firstname']);
    $user->setLastname($_POST['lastname']);
    $user->setEmail($_POST['email']);
    $result = $user->updateUser($_SESSION['user_id']);
    if($result['status']==200){
        $_SESSION['user_firstname'] = $result['data']['firstname'];
        $_SESSION['user_lastname'] = $result['data']['lastname'];
        $_SESSION['user_email'] = $result['data']['email'];
    }
}
if(isset($_POST['submit-image-btn'])){
    if (isset($_FILES)) {
        if($_FILES['image']['name']){
            $fileData['image_name'] = $_FILES['image']['name'];
            $fileData['image_ext'] = strtolower(pathinfo($_FILES['image']['name'])['extension']);
            $fileData['image_content'] = $_FILES['image']['tmp_name'];
            $fileData['image_size'] = $_FILES['image']['size'];
            if (in_array($fileData['image_ext'], array('png', 'jpg', 'jpeg','gif'))) {
                if ($fileData['image_size'] < 5000000) {
                    include_once __DIR__.'/includes/User.php';
                    $user = new User();
                    $result = $user->uploadImage($_SESSION['user_id'],$fileData['image_name']);
                    if($result['status'] == 200){
                        move_uploaded_file($fileData['image_content'],'img/'.$result['image_name']);
                        $_SESSION['user_image'] = $result['image_name'];
                    }
                }
            }
        }
    }
}

include_once __DIR__.'/header.php';
include_once __DIR__.'/aside.php';
?>

<main class="body-main" id="body-main">
    <h1>Profile</h1>
    <div class="divider"></div>
    <div class="profile-main-container">
        <img src="img/<?php echo isset($_SESSION['user_image'])?$_SESSION['user_image']:'profile.png';?>" class="profile-image" alt="">
        <div class="edit-profile">
            <form class="edit-image-form" action="" method="post" enctype="multipart/form-data">
                <div class="form-element">
                    <input type="file" accept="image/*" name="image" id="image-file" onchange="viewImage(this)">
                    <button type="submit" class="submit-btn btn" id="submit-image-btn" name="submit-image-btn">Upload</button>
                </div>
            </form>
            <div class="divider">

            </div>
            <form class="edit-profile-form" id="edit-profile-form" action="" method="post">
                <div class="form-element">
                    <label for="">First Name: </label>
                    <input type="text" name="firstname" value="<?php echo $_SESSION['user_firstname']?$_SESSION['user_firstname']:''; ?>" placeholder="First Name">
                </div>
                <div class="form-element">
                    <label for="">Last Name: </label>
                    <input type="text" name="lastname" value="<?php echo $_SESSION['user_lastname']?$_SESSION['user_lastname']:''; ?>" placeholder="Last Name">
                </div>
                <div class="form-element">
                    <label for="">Email: </label>
                    <input type="text" name="email" value="<?php echo $_SESSION['user_email']?$_SESSION['user_email']:''; ?>" placeholder="email">
                </div>
                <div class="form-element">
                    <button type="submit" class="submit-btn btn" id="submit-btn" name="submit-info-btn">Submit</button>
                </div>
            </form>

        </div>
        <div class="change-password">
            <form id="change-password-form" class="change-password-form" action="" method="post" autocomplete="no-do">
                <div class="form-element">
                    <label for="">Old Password: </label>
                    <input type="password" name="old-password" value="" placeholder="old password">
                </div>
                <div class="form-element">
                    <label for="">New Password: </label>
                    <input type="password" name="new-password" value="" placeholder="new password">
                </div>
                <div class="form-element">
                    <label for="">RePassword: </label>
                    <input type="password" name="re-password" value="" placeholder="retype password">
                </div>
                <div class="form-element">
                    <button type="submit" class="submit-btn btn" id="submit-password-btn" name="submit-password-btn">Change</button>
                </div>
            </form>
        </div>
    </div>

</main>
<?php
include_once 'footer.php';
include_once 'scripts_and_end_page.php';
?>
