<?php
include_once 'php_head.php';
$pageName = "request_delivery";
$title = "Pharmacy Delivery";
$dicription = "";
if($currentStage||$_SESSION['user_role']=="Madaba"){
    header("location: index.php");
}
if(isset($_POST['type'])){
    if($_POST['type']=='request_delivery'){
        $content = array();
        $length = sizeof($_POST['medicine']);
        for ($i=0; $i < $length; $i++) {
            $content[] = array($_POST['medicine'][$i],$_POST['quantity'][$i]);
        }

        $innerDelivery->insertRequest($_SESSION['user_id'],$content,$_POST['note']);
        header("location: index.php");
    }
}

include_once 'header.php';
include_once 'aside.php';
?>

<main class="body-main" id="body-main">
    <h1>Request Delivery</h1>
    <div class="divider"></div>
    <form class="add-form" id="request-delivery-form" action="" method="post">
        <input type="hidden" name="type" value="request_delivery">
        <fieldset>
            <legend>List</legend>
            <div class="form-element list">
                <label for="">Medicine:</label>
                <select class="medicine-select" name="medicine[]">
                </select>
                <input type="text" name="quantity[]" value="" placeholder="number of boxes">
            </div>
        </fieldset>
        <div class="form-element list">
            <label for="">Note:</label>
            <textarea name="note" cols="80" placeholder="notes"></textarea>
        </div>
    </form>
    <div class="form-element">
        <button type="button" class="add-btn btn" id="add-element" name="add">Add more</button>
        <button type="button" class="submit-btn btn" id="submit-btn" name="submitbtn">Submit</button>
    </div>

</main>

<?php
include_once 'footer.php';
include_once 'scripts_and_end_page.php';
?>
