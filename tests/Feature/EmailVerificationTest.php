<?php

namespace Tests\Feature;

use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Verified;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\URL;
use Tests\Feature\Helper\TestUserSetup;
use Tests\TestCase;

class EmailVerificationTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        // ...
        TestUserSetup::createTestUser();
    }

    public function tearDown(): void
    {

        TestUserSetup::removeTestData();
    }

    public function test_email_verification_screen_can_be_rendered()
    {
        $email_verified_at = [
            'email_verified_at' => null,
        ];
        $user = TestUserSetup::getTestUser($email_verified_at);

        echo ($user->email_verified_at);
        $response = $this->actingAs($user)->get('/verify-email');

        $response->assertStatus(200);
    }

    public function test_email_can_be_verified()
    {
        Event::fake();

        $user = TestUserSetup::getTestUser([
            'email_verified_at' => null,
        ]);

        $verificationUrl = URL::temporarySignedRoute(
            'verification.verify',
            now()->addMinutes(60),
            ['id' => $user->id, 'hash' => sha1($user->email)]
        );

        $response = $this->actingAs($user)->get($verificationUrl);

        Event::assertDispatched(Verified::class);
        $this->assertTrue($user->fresh()->hasVerifiedEmail());
        $response->assertRedirect(RouteServiceProvider::HOME . '?verified=1');
    }

    public function test_email_is_not_verified_with_invalid_hash()
    {
        $user = TestUserSetup::getTestUser([
            'email_verified_at' => null,
        ]);

        $verificationUrl = URL::temporarySignedRoute(
            'verification.verify',
            now()->addMinutes(60),
            ['id' => $user->id, 'hash' => sha1('wrong-email')]
        );

        $this->actingAs($user)->get($verificationUrl);

        $this->assertFalse($user->fresh()->hasVerifiedEmail());
    }
}
