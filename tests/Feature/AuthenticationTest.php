<?php

namespace Tests\Feature;

use App\Providers\RouteServiceProvider;
use Ramsey\Uuid\FeatureSet;
use Tests\Feature\Helper\TestUserSetup;
use Tests\TestCase;

class AuthenticationTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        // ...
        TestUserSetup::createTestUser();
    }

    public function tearDown(): void
    {

        TestUserSetup::removeTestData();
    }


    public function test_login_screen_can_be_rendered()
    {
        $response = $this->get('/login');

        $response->assertStatus(200);
    }

    public function test_users_can_authenticate_using_the_login_screen()
    {
        $user = TestUserSetup::getTestUser();

        $response = $this->post('/login', [
            'email' => $user->email,
            'password' => 'password',
        ]);

        $this->assertAuthenticated();
        $response->assertRedirect(RouteServiceProvider::HOME);
    }

    public function test_users_can_not_authenticate_with_invalid_password()
    {
        $user = TestUserSetup::getTestUser();

        $this->post('/login', [
            'email' => $user->email,
            'password' => 'wrong-password',
        ]);

        $this->assertGuest();
    }
}
