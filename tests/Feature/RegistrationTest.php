<?php

namespace Tests\Feature;

use App\Providers\RouteServiceProvider;
use Tests\Feature\Helper\TestUserSetup;
use Tests\TestCase;

class RegistrationTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        TestUserSetup::createTestUser();
    }

    public function tearDown(): void
    {
        TestUserSetup::removeTestData();
    }

    public function test_registration_screen_can_be_rendered()
    {
        $response = $this->get('/register');

        $response->assertStatus(200);
    }

    // public function test_new_users_can_register()
    // {

    //     $response = $this->post('/register', [
    //         'username' => 'Test User',
    //         'email' => 'test@example.com',
    //         'password' => 'password',
    //         'password_confirmation' => 'password',
    //     ]);

    //     $this->assertAuthenticated();
    //     $response->assertRedirect(RouteServiceProvider::HOME);
    // }
}
