<?php

namespace Tests\Feature\Helper;

use App\Models\Location;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class TestUserSetup
{
    public static function createAndGetTestRole(): Role
    {
        return Role::firstOrCreate([
            'name' => 'test_role',
            'access_level' => 100,
        ]);
    }

    public static function createAndGetTestLocation(): Location
    {
        return Location::firstOrCreate(['name' => 'test_location']);
    }

    public static function createTestUser(): void
    {

        TestUserSetup::removeTestData();
        $roleId = self::createAndGetTestRole()->id;
        $locationId = self::createAndGetTestLocation()->id;
        User::factory()->isTestUser()->create();
    }

    public static function removeTestData(): void
    {
        User::where('username', 'test_user')->delete();
        Location::where('name', 'test_location')->delete();
        Role::where('name', 'test_role')->delete();
    }

    public static function getTestUser(array $updateState = null): User
    {
        $user = User::where('username', 'test_user')->limit(1)->get()->first();
        if ($updateState) {
            foreach ($updateState as $key => $value) {
                $user->$key = $value;
            }
            $user->save();
        }
        return $user;
    }
}
