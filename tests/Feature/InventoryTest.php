<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Feature\Helper\TestUserSetup;
use Tests\TestCase;

class InventoryTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        // ...
        TestUserSetup::createTestUser();
    }

    public function tearDown(): void
    {

        TestUserSetup::removeTestData();
    }

    public function test_inventory_page_can_be_rendered()
    {
        $user = TestUserSetup::getTestUser();
        $response = $this->actingAs($user)->get('/inventory');

        $response->assertStatus(200);
    }
}
