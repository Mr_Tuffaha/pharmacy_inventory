<?php

namespace Tests\Unit;

use App\Models\Medicine;
use Tests\TestCase;

use function PHPUnit\Framework\assertEquals;

class MedicineTest extends TestCase
{
    public function testGetAmountOfUnitsInOneBox()
    {
        $medicine = Medicine::factory()->make([
            'sachets_in_box' => 1,
            'units_in_sachet' => 1
        ]);

        assertEquals($medicine->getAmountOfUnitsInOneBox(), 1);

        $medicine = Medicine::factory()->make([
            'sachets_in_box' => 1,
            'units_in_sachet' => 2
        ]);

        assertEquals($medicine->getAmountOfUnitsInOneBox(), 2);

        $medicine = Medicine::factory()->make([
            'sachets_in_box' => 3,
            'units_in_sachet' => 10
        ]);

        assertEquals($medicine->getAmountOfUnitsInOneBox(), 30);

        $medicine = Medicine::factory()->make([
            'sachets_in_box' => 5,
            'units_in_sachet' => 5
        ]);

        assertEquals($medicine->getAmountOfUnitsInOneBox(), 25);
    }


    public function testHasMoreThanOneUnitInOneBox()
    {
        $medicine = Medicine::factory()->make([
            'sachets_in_box' => 1,
            'units_in_sachet' => 1
        ]);

        assertEquals($medicine->hasMoreThanOneUnitInOneBox(), false);

        $medicine = Medicine::factory()->make([
            'sachets_in_box' => 1,
            'units_in_sachet' => 2
        ]);

        assertEquals($medicine->hasMoreThanOneUnitInOneBox(), true);

        $medicine = Medicine::factory()->make([
            'sachets_in_box' => 3,
            'units_in_sachet' => 10
        ]);

        assertEquals($medicine->hasMoreThanOneUnitInOneBox(), true);

        $medicine = Medicine::factory()->make([
            'sachets_in_box' => 5,
            'units_in_sachet' => 5
        ]);

        assertEquals($medicine->hasMoreThanOneUnitInOneBox(), true);
    }
}
