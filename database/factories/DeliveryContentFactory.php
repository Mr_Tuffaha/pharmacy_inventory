<?php

namespace Database\Factories;

use App\Models\DeliveryContent;
use Illuminate\Database\Eloquent\Factories\Factory;

class DeliveryContentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = DeliveryContent::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
