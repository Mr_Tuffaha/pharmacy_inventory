<?php

namespace Database\Factories;

use App\Models\Delivery;
use App\Models\Location;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class DeliveryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Delivery::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $pharmacistRoleId = Role::where('access_level', 100)->limit(1)->get()->first()->id;
        $pharmacistId = User::where('role_id', $pharmacistRoleId)->get()->random()->id;
        return [
            //
            'id_in_receipt' => $this->faker->regexify('[0-9a-zA-Z]{10}'),
            'id_in_notebook' => $this->faker->regexify('[0-9a-zA-Z]{10}'),
            'deliverer' => $this->faker->company(),
            'recipient_user_id' => $pharmacistId,
            'location_id' => Location::all()->random()->first()->id,
        ];
    }
}
