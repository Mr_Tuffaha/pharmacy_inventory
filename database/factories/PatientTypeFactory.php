<?php

namespace Database\Factories;

use App\Models\PatientType;
use Illuminate\Database\Eloquent\Factories\Factory;

class PatientTypeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PatientType::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->unique()->word(),
        ];
    }
}
