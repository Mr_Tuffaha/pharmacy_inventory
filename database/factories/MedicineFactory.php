<?php

namespace Database\Factories;

use App\Models\Medicine;
use App\Models\MedicineType;
use Illuminate\Database\Eloquent\Factories\Factory;

class MedicineFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Medicine::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        extract($this->prepareMedicineContent());
        return [
            //
            'name' => $this->faker->unique()->sentence(3),
            'medicine_type_id' => MedicineType::all()->random(1)->first(),
            'sachets_in_box' => $sachets_in_box,
            'units_in_sachet' => $units_in_sachet,
        ];
    }

    protected function prepareMedicineContent(): array
    {
        $sachets_in_box = rand(1, 5);
        $units_in_sachet = rand(1, 10);
        $has_more_than_one_unit_in_box = $sachets_in_box * $units_in_sachet > 1 ? true : false;
        return [
            'sachets_in_box' => $sachets_in_box,
            'units_in_sachet' => $units_in_sachet
        ];
    }
}
