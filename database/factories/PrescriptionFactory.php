<?php

namespace Database\Factories;

use App\Models\Location;
use App\Models\PatientType;
use App\Models\Prescription;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class PrescriptionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Prescription::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $pharmacistRoleId = Role::where('access_level',100)->limit(1)->get()->first()->id;
        $pharmacistId = User::where('role_id',$pharmacistRoleId)->get()->random()->id;
        return [
            'id_on_paper' => $this->faker->regexify('[0-9a-zA-Z]{10}'),
            'patient_id' => $this->faker->regexify('[0-9a-zA-Z]{10}'),
            'patient_type_id' => PatientType::all()->random()->id,
            'prescriber_id' => $pharmacistId,
            'location_id' => Location::all()->random(),
            'note' => rand(1,100) > 80 ? $this->faker->sentence(6) : null,
        ];
    }
}
