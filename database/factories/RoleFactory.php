<?php

namespace Database\Factories;

use App\Models\Role;
use Illuminate\Database\Eloquent\Factories\Factory;

class RoleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Role::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word(),
            'access_level' => 1,
        ];
    }

    public function isNewUser()
    {
        return $this->state(function (array $attributes) {
            return [
                'name' => 'new_user',
                'access_level' => 1,
            ];
        });
    }

    public function isPharmacist()
    {
        return $this->state(function (array $attributes) {
            return [
                'name' => 'pharmacist',
                'access_level' => 100,
            ];
        });
    }

    public function isAdmin()
    {
        return $this->state(function (array $attributes) {
            return [
                'name' => 'admin',
                'access_level' => 1000,
            ];
        });
    }
}
