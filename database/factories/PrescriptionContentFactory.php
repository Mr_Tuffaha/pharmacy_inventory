<?php

namespace Database\Factories;

use App\Models\PrescriptionContent;
use Illuminate\Database\Eloquent\Factories\Factory;

class PrescriptionContentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PrescriptionContent::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
