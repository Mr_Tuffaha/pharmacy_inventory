<?php

namespace Database\Factories;

use App\Models\MedicineType;
use Illuminate\Database\Eloquent\Factories\Factory;

class MedicineTypeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = MedicineType::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'name' => $this->faker->unique()->word()
        ];
    }
}
