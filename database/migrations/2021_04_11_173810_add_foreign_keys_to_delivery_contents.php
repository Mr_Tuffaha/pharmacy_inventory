<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToDeliveryContents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('delivery_contents', function (Blueprint $table) {
            //

            $table->foreign('delivery_id')->references('id')->on('deliveries');
            $table->foreign('medicine_id')->references('id')->on('medicines');
            $table->unique(['medicine_id', 'delivery_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('delivery_contents', function (Blueprint $table) {
            //
            $table->dropForeign('delivery_contents_delivery_id_foreign');
            $table->dropForeign('delivery_contents_medicine_id_foreign');
        });
    }
}
