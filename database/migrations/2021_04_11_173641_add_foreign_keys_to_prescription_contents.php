<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToPrescriptionContents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('prescription_contents', function (Blueprint $table) {
            //

            $table->foreign('prescription_id')->references('id')->on('prescriptions');
            $table->foreign('medicine_id')->references('id')->on('medicines');
            $table->unique(['medicine_id', 'prescription_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('prescription_contents', function (Blueprint $table) {
            //
            $table->dropForeign('prescription_contents_prescription_id_foreign');
            $table->dropForeign('prescription_contents_medicine_id_foreign');
        });
    }
}
