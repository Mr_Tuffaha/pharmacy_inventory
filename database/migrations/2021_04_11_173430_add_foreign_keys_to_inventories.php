<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToInventories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inventories', function (Blueprint $table) {
            //

            $table->foreign('medicine_id')->references('id')->on('medicines');
            $table->foreign('location_id')->references('id')->on('locations');
            $table->unique(['medicine_id', 'location_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inventories', function (Blueprint $table) {
            //
            $table->dropForeign('inventories_medicine_id_foreign');
            $table->dropForeign('inventories_location_id_foreign');
        });
    }
}
