<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Role::firstOrCreate([
            'name' => 'new_user',
            'access_level' => 1,
        ]);

        //
        Role::firstOrCreate([
            'name' => 'pharmacist',
            'access_level' => 100,
        ]);

        Role::firstOrCreate([
            'name' => 'admin',
            'access_level' => 1000,
        ]);
    }
}
