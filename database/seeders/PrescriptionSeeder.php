<?php

namespace Database\Seeders;

use App\Models\Medicine;
use App\Models\PatientType;
use App\Models\Prescription;
use App\Models\PrescriptionContent;
use Illuminate\Database\Seeder;


class PrescriptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        PatientType::factory()->count(5)->create();
        Prescription::factory()->count(100)->create()->each(function($prescription){
            $medicineList = Medicine::all(['id','sachets_in_box','units_in_sachet'])->random(rand(1,5));
            foreach($medicineList as $medicine)
            {
                PrescriptionContent::factory()->create([
                    'prescription_id' => $prescription->id,
                    'medicine_id' => $medicine->id,
                    'smallest_unit_count' => $medicine->sachets_in_box * $medicine->units_in_sachet * rand(1,12),
                ]);
            }
        });
    }
}
