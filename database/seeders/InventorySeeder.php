<?php

namespace Database\Seeders;

use App\Models\Inventory;
use App\Models\Location;
use App\Models\Medicine;
use Illuminate\Database\Seeder;

class InventorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // $table->unsignedBigInteger('medicine_id');
        // $table->integer('smallest_unit_count');
        // $table->unsignedBigInteger('location_id');
        Location::all(['id'])->each(function($location){
            Medicine::all(['id','sachets_in_box','units_in_sachet'])->each(function($medicine) use ($location){
                Inventory::factory()->create([
                    'medicine_id' => $medicine->id,
                    'smallest_unit_count' => $medicine->sachets_in_box * $medicine->units_in_sachet * rand(5,20),
                    'location_id' => $location->id,
                ]);
            });

        });
    }
}
