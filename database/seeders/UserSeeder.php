<?php

namespace Database\Seeders;

use App\Models\Location;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $roleId = Role::where('name', 'new_user')->first()->id;
        $locationId = Location::limit(1)->get()->first()->id;
        User::firstOrCreate(
            [
                'username' => 'guest',
                'email' => 'guest@example.com',
                'firstname' => 'firstname',
                'lastname' => 'lastname',
            ],
            [
                'username' => 'guest',
                'email' => 'guest@example.com',
                'password' => Hash::make('password'),
                'email_verified_at' => now(),
                'firstname' => 'firstname',
                'lastname' => 'lastname',
                'role_id' => $roleId,
                'location_id' => $locationId,
            ]
        );

        $roleId = Role::where('name', 'pharmacist')->first()->id;
        User::firstOrCreate(
            [
                'username' => 'pharmacist',
                'email' => 'pharmacist@example.com',
                'firstname' => 'firstname',
                'lastname' => 'lastname',
            ],
            [
                'username' => 'pharmacist',
                'email' => 'pharmacist@example.com',
                'password' => Hash::make('password'),
                'email_verified_at' => now(),
                'firstname' => 'firstname',
                'lastname' => 'lastname',
                'role_id' => $roleId,
                'location_id' => $locationId,
            ]
        );

        $roleId = Role::where('name', 'admin')->first()->id;
        User::firstOrCreate(
            [
                'username' => 'admin',
                'email' => 'admin@example.com',
                'firstname' => 'firstname',
                'lastname' => 'lastname',
            ],
            [
                'username' => 'admin',
                'email' => 'admin@example.com',
                'password' => Hash::make('password'),
                'email_verified_at' => now(),
                'firstname' => 'firstname',
                'lastname' => 'lastname',
                'role_id' => $roleId,
                'location_id' => $locationId,
            ]
        );
    }
}
