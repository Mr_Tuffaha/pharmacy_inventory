<?php

namespace Database\Seeders;

use App\Models\Medicine;
use App\Models\MedicineType;
use Illuminate\Database\Seeder;

class MedicineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MedicineType::factory()->count(10)->create();
        Medicine::factory()->count(100)->create();
    }
}
