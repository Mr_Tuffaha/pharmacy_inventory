<?php

namespace Database\Seeders;

use App\Models\Delivery;
use App\Models\DeliveryContent;
use App\Models\Medicine;
use Illuminate\Database\Seeder;

class DeliverySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //


        Delivery::factory()->count(100)->create()->each(function($delivery){
            $medicineList = Medicine::all(['id','sachets_in_box','units_in_sachet'])->random(rand(5,30));
            foreach($medicineList as $medicine)
            {
                DeliveryContent::factory()->create([
                    'delivery_id' => $delivery->id,
                    'medicine_id' => $medicine->id,
                    'smallest_unit_count' => $medicine->sachets_in_box * $medicine->units_in_sachet * rand(5,20),
                ]);
            }
        });
    }
}
