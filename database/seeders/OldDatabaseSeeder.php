<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class OldDatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call([
            OldData\LocationsSeeder::class,
            OldData\MedicineSeeder::class,
            OldData\InventorySeeder::class,
            RoleSeeder::class,
            UserSeeder::class,
            OldData\PrescriptionSeeder::class,
            OldData\DeliverySeeder::class,
        ]);

    }
}
