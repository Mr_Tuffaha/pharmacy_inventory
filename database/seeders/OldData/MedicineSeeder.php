<?php

namespace Database\Seeders\OldData;

use Illuminate\Database\Seeder;
use App\Models\Medicine;
use App\Models\MedicineType;
use Illuminate\Support\Facades\Storage;


class MedicineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $inventory = Storage::get('olddata/inventory.csv');
        $inventoryRowArray = explode('\n',trim(json_encode($inventory),"\""));
        $inventoryArray = [];
        foreach( $inventoryRowArray as $row){
            if($row){
                $inventoryArray[] = str_getcsv($row,',',"'");
            }
        }

        foreach( $inventoryArray as $row){
            $medicineType = MedicineType::firstOrCreate([
                'name'=>$row[2]
            ]);
            $hasMoreThanOneUnit = $row[3]+$row[4] > 2 ? true : false;
            Medicine::firstOrCreate([
                'name' => $row[1],
                'medicine_type_id' => $medicineType->id,
                'has_more_than_one_unit_in_box' => $hasMoreThanOneUnit,
                'sachets_in_box' => $row[3],
                'units_in_sachet' => $row[4],
            ]);
        }

    }
}
