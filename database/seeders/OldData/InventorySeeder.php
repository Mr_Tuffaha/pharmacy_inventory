<?php

namespace Database\Seeders\OldData;

use App\Models\Inventory;
use App\Models\Location;
use App\Models\Medicine;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class InventorySeeder extends Seeder
{
    private $inventoryArray;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $inventory = Storage::get('olddata/inventory.csv');
        $inventoryRowArray = explode('\n',trim(json_encode($inventory),"\""));
        $this->inventoryArray = [];
        foreach( $inventoryRowArray as $row){
            if($row){
                $this->inventoryArray[] = str_getcsv($row,',',"'");
            }
        }
        $inventoryContent = Storage::get('olddata/inventory_content.csv');
        $inventoryContentRowArray = explode('\n',trim(json_encode($inventoryContent),"\""));
        $inventoryContentArray = [];
        foreach( $inventoryContentRowArray as $row){
            if($row) {
                $inventoryContentArray[] = str_getcsv($row, ',', "'");
            }
        }

        foreach($inventoryContentArray as $row)
        {
            $name = $this->medicineNameFromId($row[1]);
            $medicineId = Medicine::where('name', $name)->first()?->id;
            $locationId = Location::where('name',$row[2])->first()?->id;
            if($medicineId && $locationId){
                Inventory::firstOrCreate([
                    'medicine_id' => $medicineId,
                    'smallest_unit_count' => $row[3],
                    'location_id' => $locationId,
                ]);
            }

        }
    }

    public function medicineNameFromId($medicineId)
    {
        foreach($this->inventoryArray as $row)
        {
            if($row[0] == $medicineId){
                return $row[1];
            }
        }
    }
}
