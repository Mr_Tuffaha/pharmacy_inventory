<?php

namespace Database\Seeders\OldData;

use Illuminate\Database\Seeder;

use App\Models\Location;

class LocationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Location::firstOrCreate(['name'=>'Madaba']);
        Location::firstOrCreate(['name'=>'Jabal Amman']);
    }
}
