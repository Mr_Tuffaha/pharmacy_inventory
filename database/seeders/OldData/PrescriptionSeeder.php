<?php

namespace Database\Seeders\OldData;

use App\Models\Location;
use App\Models\Medicine;
use App\Models\PatientType;
use App\Models\Prescription;
use App\Models\PrescriptionContent;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class PrescriptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    protected $prescriptionArray;
    protected $prescriptionContentArray;

    public function run()
    {
        $this->prescriptionArray = $this->prepareDataArray('olddata/prescriptions.csv');
        $this->prescriptionContentArray = $this->prepareDataArray('olddata/prescription_content.csv');
        $this->fillprescriptionTable();
        $this->fillprescriptionContentTable();

    }

    public function fillprescriptionTable()
    {

        $patientTypeId = [];
        $locationId = [];
        foreach($this->prescriptionArray as $row){
            if(!isset($patientTypeId[$row[3]]))
            {
                $patientTypeId[$row[3]] = $this->getOrCreatePatietTypeId($row[3]);
            }

            if(!isset($locationId[$row[6]]))
            {
                $locationId[$row[6]] = $this->getLocationIdByName($row[6]);
            }
            Prescription::firstOrCreate([
                'id_on_paper'=>$row[1],
                'patient_id'=>$row[2],
                'patient_type_id'=>$patientTypeId[$row[3]],
                'perscriber_id'=>1,
                'location_id'=>$locationId[$row[6]],
                'note'=>$row[7],
                'created_at'=>$row[4],
                'updated_at'=>$row[4],
            ]);
        }
    }

    public function fillPrescriptionContentTable()
    {

        $prescriptionId = [];
        $medicineId = [];
        foreach($this->prescriptionContentArray as $row){


            if(!isset($medicineId[$row[2]]))
            {
                $medicineId[$row[2]] = $this->getMedicineIdByName($row[2]);
            }

            if(!isset($prescriptionId[$row[1]]))
            {
                $prescriptionId[$row[1]] = $this->getPrescriptionIdByOldId($row[1]);
            }
            PrescriptionContent::firstOrCreate([
                'prescription_id' => $prescriptionId[$row[1]],
                'medicine_id' => $medicineId[$row[2]],
                'smallest_unit_count' => $row[3],
            ]);
        }
    }

    public function prepareDataArray($csvFilePath)
    {
        $data = Storage::get($csvFilePath);
        $dataRowArray = explode('\n',trim(json_encode($data),"\""));
        $dataArray = [];
        foreach( $dataRowArray as $row){
            if($row){
                $dataArray[] = str_getcsv($row,',',"'");
            }
        }
        return $dataArray;
    }

    public function getOrCreatePatietTypeId($patietTypeName)
    {
        return PatientType::firstOrCreate([
            'name' =>  $patietTypeName,
        ])?->id;
    }

    public function getLocationIdByName($patietTypeName)
    {
        return Location::where('name',$patietTypeName)->first()?->id;
    }

    public function getMedicineIdByName($medicineName)
    {
        $medicineName = $medicineName=='zeimax'?$medicineName." 250":$medicineName;
        return Medicine::where('name',$medicineName)->first()?->id;
    }

    public function getPrescriptionIdByOldId($persrciptionOldId)
    {
        $key = array_search($persrciptionOldId, array_column($this->prescriptionArray, 0));
        $oldPrescriptionData = $this->prescriptionArray[$key];
        return Prescription::where([
            'id_on_paper'=> $oldPrescriptionData[1],
            'patient_id' => $oldPrescriptionData[2],
            'patient_type_id' => $this->getOrCreatePatietTypeId($oldPrescriptionData[3]),
            'perscriber_id' => 1,
            'location_id' => $this->getLocationIdByName($oldPrescriptionData[6]),
            'note' => $oldPrescriptionData[7],
            'created_at' => $oldPrescriptionData[4],
            'updated_at' => $oldPrescriptionData[4],
        ])->first()?->id;
    }
}
