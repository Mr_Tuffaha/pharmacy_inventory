<?php

namespace Database\Seeders\OldData;

use App\Models\Delivery;
use App\Models\DeliveryContent;
use App\Models\Medicine;
use App\Models\PatientType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class DeliverySeeder extends Seeder
{

    protected $deliveryArray;
    protected $deliveryContentArray;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $this->deliveryArray = $this->prepareDataArray('olddata/deliveries.csv');
        $this->deliveryContentArray = $this->prepareDataArray('olddata/delivery_contents.csv');
        $this->fillDeliveryTable();
        $this->fillDeliveryContentTable();
    }



    public function prepareDataArray($csvFilePath)
    {
        $data = Storage::get($csvFilePath);
        $dataRowArray = explode('\n', trim(json_encode($data), "\""));
        $dataArray = [];
        foreach ($dataRowArray as $row) {
            if ($row) {
                $dataArray[] = str_getcsv($row, ',', "'");
            }
        }
        return $dataArray;
    }

    public function fillDeliveryTable()
    {
        foreach ($this->deliveryArray as $row) {

            Delivery::firstOrCreate([
                'id_in_receipt' => $row[1],
                'id_in_notebook' => $row[2],
                'deliverer' => $row[4],
                'recipient_user_id' => 1,
                'location_id' => 1,
                'created_at' => $row[3],
                'updated_at' => $row[3],
            ]);
        }
    }

    public function fillDeliveryContentTable()
    {
        $medicineId = [];
        $deliveryId = [];
        foreach ($this->deliveryContentArray as $row) {

            $medicineId[$row[2]] = !isset($medicineId[$row[2]]) ? $this->getMedicineIdByName($row[2]) : $medicineId[$row[2]];
            $deliveryId[$row[1]] = !isset($deliveryId[$row[1]]) ? $this->getDeliveryIdByOldId($row[1]) : $deliveryId[$row[1]];
            DeliveryContent::firstOrCreate([
                'delivery_id' => $deliveryId[$row[1]],
                'medicine_id' => $medicineId[$row[2]],
                'smallest_unit_count' => $row[3],
            ]);
        }
    }

    public function getDeliveryIdByOldId($deliveryOldId)
    {
        $key = array_search($deliveryOldId, array_column($this->deliveryArray, 2));
        $oldDeliveryData = $this->deliveryArray[$key];
        return Delivery::where([
            'id_in_receipt' => $oldDeliveryData[1],
            'id_in_notebook' => $oldDeliveryData[2],
            'deliverer' => $oldDeliveryData[4],
            'recipient_user_id' => 1,
            'location_id' => 1,
            'created_at' => $oldDeliveryData[3],
            'updated_at' => $oldDeliveryData[3],
        ])->first()?->id;
    }

    public function getMedicineIdByName($medicineName)
    {
        $medicineName = $medicineName == 'zeimax' ? $medicineName . " 250" : $medicineName;
        return Medicine::where('name', $medicineName)->first()?->id;
    }
}
